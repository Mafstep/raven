FROM openjdk:8-jre
COPY /target/raven-*.jar /app/raven.jar
WORKDIR /app
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "raven.jar"]

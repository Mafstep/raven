# Raven [![pipeline status](http://gitlab.jibit.ir/backend/raven/badges/master/pipeline.svg)](http://gitlab.jibit.ir/backend/raven/commits/master) [![coverage report](http://gitlab.jibit.ir/backend/raven/badges/master/coverage.svg)](http://gitlab.jibit.ir/backend/raven/commits/master)
Raven is a Notification as a Service for Jibit.

## Getting Started
Please follow these instructions to have Raven up and running on your local machine for development and testing purposes.

### Prerequisites
No dependencies are required other than JDK 1.8+, everything is integrated into the build toolchain.

### Development Profile
For development purposes, please activate the `dev` profile on your favorite IDE.

#### Intellij
If your using Intellij, you can change your profile to `dev` by adding it in `active profile` section 
of the `Run configuration`. 

### Dependent Services
To fulfill its responsibilities, Raven needs the following components:
 - A MySQL instance to store its *to-be-persisted* data.
 - A RabbitMQ instance to publish notifications to.
 - Some APIs, e.g. `/v1/tokens`, need the IranPay v2.

#### Run Dependent Services and Databases
By default, Raven expects to find a MySQL instance listening on port `33060` and a RabbitMQ instance on port `56720`. 
One way to run these dependencies is to use the compose configurations in the [docker-compose](docker/docker-compose.yml) 
file. In order to do that just `up`:
```bash
cd docker
docker-compose up
```
Then the dependent components should be up and running as following:

| Service    |  Container Name   |  Port |                        How to Connect?                 |
|:----------:| :----------------:|:-----:|:------------------------------------------------------:|
|  MySQL     | `docker_mysql_1`  | 33060 | `docker exec -it docker_mysql_1 mysql -u root -p raven`|
|  RabbitMQ  | `docker_rabbit_1` | 56720 | `docker exec -it docker_rabbit_1 bash`                 |

### Run Tests
In order to just run the *Unit Tests*:
```
./mvnw clean test
```
If you're going to run *Integration Tests*, first make sure that:
 - Docker is installed and running!
 
Then you can safely run them by:
```
./mvnw clean verify
```

## Production Requirements
In order to run Raven in any new environment, other than `dev`, we need to provide a set of MySQL and RabbitMQ specific
configurations.

### PRD, SIT and UAT Profiles
We should activate the `prd`, `SIT` and `UAT` profiles in their respective environments. The simplest way to activate this 
profile is to pass the profile name on the command line:
```bash
--spring.profiles.active=prd
```
We can use the `SPRING_PROFILES_ACTIVE` environment variable, too. Also, if we're going to use the additional configuration
locations, then we should add the profile specific configuration files, e.g. `application-prd.yml`to those additional locations.

### Additional Configuration Files
If you're going to use additional configuration locations in addition to the default locations, specify that location using the
`spring.config.additional-location` configuration property.

### MySQL Configurations
In order to have a working connection to MySQL, we should provide:
 - `spring.datasource.url`: Is the JDBC URL to MySQL instance with `jdbc:mysql://{host}:{port}/{database}` format.
 - `spring.datasource.username`: Is the database username.
 - `spring.datasource.password`: IS the database password.

Optionally, we can provide connection pool specific configurations, too.

### RabbitMQ Configurations
In order to get connected to RabbitMQ, we need to provide the following configurations:
 - `spring.rabbitmq.host`: Host or IP address of RabbitMQ server.
 - `spring.rabbitmq.port`: Port number for the RabbitMQ instance.
 - `spring.rabbitmq.username`: The username of the RabbitMQ user.
 - `spring.rabbitmq.password`: The password of the RabbitMQ user.
 
If necessary, we may need to provide SSL or VHost related configurations.

### IranPay v2 Profile API
In order to turn our JWT tokens to user details, we need to call the IranPay Profile API. So we should consider setting
a property as the base URL for that API:
 - `app-server.user-profile.url`: Represents the URL for the Profile API of IranPay, e.g. `http://172.16.0.50:6000/profile`. 

### Logging
As part of our team agreement, we should append application logs to a file with a configurable path:
 - `logging.level.root`: Determines the log level, i.e. verbosity, for the `root` logger. The default log level is `INFO`.
 - `logging.path`: Location of the log file. A file named `spring.log` will be created under the specified path.

### Public APIs
We only need to expose `/v1/tokens` endpoint, other endpoints should only be accessible locally.

## Architecture
Raven is using a few components to send a particular notification:
 - The first one is the Raven itself. It acts as the notification request collectors and pushes
 each notification to an appropriate exchange.
 - A notification exchange on RabbitMQ (Can be the default exchange) which distributes incoming messages to proper
 queues.
 - A set of notification processors each with the expertise of handling one particular notification type.
 - If we fail to process a particular notification, we send that notification to *Dead Letter* exchange for future 
 notification retransmissions.

![Architecture](architecture.png)

## REST Documentation
For every successful build of Raven, the REST API documentation would be uploaded to its Gitlab Pages. Also, one can
see the API documentation locally using `docker-compose`.

### Say No to Automatic Documentation Generators
Add your API documentations to the [Swagger](swagger/swagger.yml) file manually. You can use the [official swagger editor](https://editor.swagger.io)
to facilitate the documenting process. And one more thing, good documentations are always written by hand!

### Gitlab Pages
If the last build was successful, you can checkout the REST documentation in Raven's pages at:
```
http://backend.gitlabpages.jibit.ir/raven/
```

### Docker Compose
If you're gonna see the documentations locally, first `cd` to the [swagger](swagger) directory and then just do an `up`:
```bash
cd swagger
docker-compose up
```
After that, the documentations should be available at `http://localhost:1984`. To shutdown the server:
```bash
docker-compose down
```

## Built With

* [Spring Boot](https://spring.io/projects/spring-boot) - As our main application framework.
* [Kotlin](https://kotlinlang.org) - As our primary JVM language.
* [JUnit 5](https://junit.org/junit5/) - The testing framework on JVM.
* [JoCoCo](https://github.com/jacoco/jacoco) - As our *Code Coverage* tool.
* [Flyway](https://flywaydb.org) - As our database migration tool.
* [Maven](https://maven.apache.org) - As our build tool.
* [MySQL](https://www.mysql.com) - The database we're using.
* [RabbitMQ](https://www.rabbitmq.com) - The messaging framework we're using

and last but certainly not least:
* Java & JVM

## Contributing

Please read [CONTRIBUTING](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests.

## Versioning

We use [SemVer](http://semver.org/) for versioning.

## Authors

See also the list of [CONTRIBUTORS](CONTRIBUTORS.md) who participated in this project.

## Acknowledgments

* Hat tip to anyone whose code was used, even if we totally stole it!

package ir.jibit.raven.notification

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.nhaarman.mockito_kotlin.*
import ir.jibit.raven.model.notification.EmailNotification
import ir.jibit.raven.model.notification.Notification
import ir.jibit.raven.model.notification.SmsNotification
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments.arguments
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.amqp.core.Message
import org.springframework.amqp.rabbit.core.RabbitTemplate

/**
 * Unit tests for [RabbitMqExchangeNotificationProcessor].
 *
 * @author Mehran Behnam
 */
class RabbitMqExchangeNotificationProcessorTest {

    private lateinit var processor: RabbitMqExchangeNotificationProcessor
    private lateinit var rabbitTemplate: RabbitTemplate
    private val json = ObjectMapper().registerModule(JavaTimeModule())

    @BeforeEach
    fun setUp() {
        rabbitTemplate = mock()
        processor = RabbitMqExchangeNotificationProcessor(rabbitTemplate, "queue", SmsNotification::class)
    }

    @ParameterizedTest
    @MethodSource("paramsForCanProcess")
    fun `Only can process specified notification types`(notification: Notification, expected: Boolean) {
        assertThat(processor.canProcess(notification))
                .isEqualTo(expected)
    }

    @ParameterizedTest
    @MethodSource("notifications")
    fun `When everything is OK, the notification should be published to appropriate queue`(notification: Notification) {
        processor.process(notification)

        val queueName = argumentCaptor<String>()
        val message = argumentCaptor<Message>()
        verify(rabbitTemplate).send(queueName.capture(), message.capture())

        assertThat(queueName.firstValue).isEqualTo("queue")
        assertThat(message.firstValue.body).isEqualTo(json.writeValueAsBytes(notification))
        if (notification.ttl > 0)
            assertThat(message.firstValue.messageProperties.expiration).isEqualTo("${notification.ttl * 1000}")
        else
            assertThat(message.firstValue.messageProperties.expiration).isNull()
    }

    @Test
    fun `If we fail to send a notification, an exception should be thrown`() {
        whenever(rabbitTemplate.send(any(), any())).thenThrow(RuntimeException())

        val notification = SmsNotification(recipients = setOf(""), message = "That's sad :(")
        val ex = assertThrows<NotificationProcessingFailureException> {
            processor.process(notification)
        }

        assertThat(ex.cause).isInstanceOf(RuntimeException::class.java)
        assertThat(ex.notification).isEqualTo(notification)
    }

    companion object {
        @JvmStatic fun paramsForCanProcess() = listOf(
                arguments(SmsNotification(recipients = setOf(""), message = ""), true),
                arguments(EmailNotification(recipients = setOf(""), subject = "", body = ""), false)
        )

        @JvmStatic fun notifications() = listOf(
                SmsNotification(recipients = setOf("123"), message = "123").apply { ttl = -1 },
                SmsNotification(recipients = setOf("123"), message = "123").apply { ttl = 0 },
                SmsNotification(recipients = setOf("123"), message = "123").apply { ttl = -100 },
                SmsNotification(recipients = setOf("123"), message = "123").apply { ttl = 1 },
                SmsNotification(recipients = setOf("123"), message = "123").apply { ttl = 10 },
                SmsNotification(recipients = setOf("123", "456"), message = "123")
        )
    }
}

package ir.jibit.raven.notification

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.readValue
import ir.jibit.raven.AutoConfigureRabbit
import ir.jibit.raven.model.notification.SmsNotification
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.amqp.core.Message
import org.springframework.amqp.core.Queue
import org.springframework.amqp.rabbit.core.RabbitAdmin
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ActiveProfiles

/**
 * Integration tests for [RabbitMqExchangeNotificationProcessor].
 *
 * @author Mehran Behnam
 */
@DirtiesContext
@AutoConfigureRabbit
@ActiveProfiles("test")
class RabbitMqExchangeNotificationProcessorIT {

    /**
     * Subject under test.
     */
    @Autowired private lateinit var processor: RabbitMqExchangeNotificationProcessor

    /**
     * Will be used to interact with RabbitMQ server.
     */
    @Autowired private lateinit var rabbitTemplate: RabbitTemplate

    /**
     * Will be used to purge a RabbitMQ queue.
     */
    @Autowired private lateinit var rabbitAdmin: RabbitAdmin

    /**
     * For Json marshalling/unmarshalling.
     */
    private val json = ObjectMapper().registerModule(JavaTimeModule())

    @BeforeEach
    fun `Purge the test queue`() {
        rabbitAdmin.purgeQueue("test-queue")
    }

    @Test
    fun `We should be able to publish messages to queue listeners`() {
        val sms = SmsNotification(recipients = setOf("0912 xxx"), message = "Hello")
        sms.ttl = 2
        processor.process(sms)

        val message = rabbitTemplate.receive("test-queue", 1000)
        val received = json.readValue<SmsNotification>(message!!.body)

        assertThat(received.id).isEqualTo(sms.id)
        assertThat(received.ttl).isEqualTo(sms.ttl)
        assertThat(received.data).isEqualTo(sms.data)
        assertThat(received.notificationState).isEqualTo(sms.notificationState)
        assertThat(received.processLog).isEqualTo(sms.processLog)
        assertThat(received.recipients).isEqualTo(sms.recipients)
        assertThat(received.message).isEqualTo(sms.message)
    }

    @Test
    fun `After message expiration, no one should be able to consume it`() {
        val sms = SmsNotification(recipients = setOf("0912 xxx"), message = "Hello")
        sms.ttl = 1 // Message should be expired after one second (i.e. 1000 millis)
        processor.process(sms)

        Thread.sleep(1500) // Wait until the message is expired

        val message: Message? = rabbitTemplate.receive("test-queue", 500)
        assertThat(message).isNull()
    }

    /**
     * Test configuration which registers a RabbitMQ queue and a simple implementation of subject
     * under test into Spring application context.
     */
    @TestConfiguration
    protected class RabbitMqExchangeNotificationProcessorTestConfig {

        @Bean
        fun testQueue() = Queue("test-queue")

        @Bean
        fun processor(rabbitTemplate: RabbitTemplate) =
                RabbitMqExchangeNotificationProcessor(rabbitTemplate, "test-queue", SmsNotification::class)
    }
}

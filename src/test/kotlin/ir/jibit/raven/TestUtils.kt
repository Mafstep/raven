package ir.jibit.raven

import org.assertj.core.api.AbstractAssert
import org.assertj.core.api.Condition
import org.assertj.core.api.IterableAssert
import java.util.function.Predicate

/**
 * Useful when we're gonna send an empty body as the request body.
 *
 * @author Mehran Behnam
 */
fun emptyBody() = emptyMap<String, Any>()

/**
 * Useful when we're going to send JSON key-values as the request body for
 * a particular endpoint.
 *
 * @author Mehran Behnam
 */
fun <K, V> body(vararg pairs: Pair<K, V>): Map<K, V> = mapOf(*pairs)

/**
 * A more idiomatic way to define a `has` assertion on collections of things. Instead of:
 * ```
 *     assertThat(someCollection).has(Condition(Predicate { thePredicate }, message))
 * ```
 * We can write:
 * ```
 *     assertThat(someCollection).has(message) {
 *         thePredicate
 *     }
 * ```
 */
fun <T> IterableAssert<T>.has(description: String, condition: Iterable<T>.() -> Boolean):
        AbstractAssert<IterableAssert<T>, MutableIterable<T>> = has(Condition(Predicate { condition(it) }, description))

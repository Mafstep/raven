package ir.jibit.raven

import com.spotify.docker.client.DefaultDockerClient
import com.spotify.docker.client.DockerClient
import com.spotify.docker.client.DockerClient.RemoveContainerParam
import com.spotify.docker.client.messages.ContainerConfig
import com.spotify.docker.client.messages.HostConfig
import com.spotify.docker.client.messages.PortBinding
import ir.jibit.raven.util.logger
import org.junit.jupiter.api.extension.AfterAllCallback
import org.junit.jupiter.api.extension.BeforeAllCallback
import org.junit.jupiter.api.extension.ExtensionContext

/**
 * A JUnit extension responsible for setting a docker container up before a test class and
 * tearing it down after test class. This implementation requires a docker engine to be able
 * to create docker containers:
 *  - [image] is the docker image name.
 *  - [tag] is the docker image tag.
 *  - [env] List of environment variables in the `name=value` format.
 *  - [wait] A function which will be called after starting the container. Usually you can
 *  wait for the container to complete its startup phase.
 *  - [portMappings] determines how we want to map container ports to host ports.
 *
 * In order to setup a docker container for test execution, define a JUnit extension like following:
 * ```
 * class SomeTest {
 *    companion object {
 *        @JvmField @RegisterExtension
 *        val mongoServer = DockerContainer("mongo", "3.6", PortMapping(12345, 27017))
 *    }
 * }
 * ```
 * This would run a MongoDb instance on port `12345` before the test class and shut it down after
 * running all tests.
 *
 * @author Mehran Behnam
 */
open class DockerContainer(private val image: String,
                           private val tag: String = "latest",
                           private val env: List<String>,
                           private val wait: () -> Unit,
                           private vararg val portMappings: PortMapping) : BeforeAllCallback, AfterAllCallback {

    /**
     * A simpler constructor with a no-op wait strategy and no environment variables.
     */
    constructor(image: String, tag: String, vararg portMappings: PortMapping) :
            this(image, tag, emptyList<String>(), {}, *portMappings)

    /**
     * The logger.
     */
    private val log = logger<DockerContainer>()

    /**
     * Connects to the docker engine in the host OS, throw an exception if could't find
     * the docker engine.
     */
    private val docker: DockerClient = getDockerClient()

    /**
     * Id of the created container.
     */
    private var containerId: String? = null

    /**
     * Starts the docker container before the test class.
     */
    override fun beforeAll(context: ExtensionContext) {
        try {
            if (docker.listImages().none { "$image:$tag" in it.repoTags() }) {
                log.info("Couldn't find the $image:$tag image, pulling...")
                docker.pull("$image:$tag") {
                    if (it.status() == "Pull complete") log.info("Image $image:$tag pulled completely!")
                }
            }

            val portBindings = getPortBindings()
            val hostConfig = HostConfig.builder().portBindings(portBindings).build()
            val config = containerConfig(hostConfig)
            val creation = docker.createContainer(config)

            containerId = creation.id()
            docker.startContainer(containerId)

            log.debug("Waiting for the $image:$tag container to start")
            wait()
            log.debug("The $image:$tag is ready!")
        } catch (e: Exception) {
            log.error("Failed to start the docker container: $image:$tag with $portMappings", e)
            throw e
        }
    }

    /**
     * Terminates the docker container after all tests inside the test class has
     * been executed.
     */
    override fun afterAll(context: ExtensionContext) {
        log.debug("Removing the docker container $image:$tag")

        // TODO: We should register a JVM shutdown hook to remove the running docker container if
        // TODO: for some reason the after method did not get a chance for execution.
        containerId?.let {
            val forceKill = RemoveContainerParam.forceKill(true)
            docker.removeContainer(containerId, forceKill)
        }

        docker.close()
    }

    private fun getPortBindings(): Map<String, List<PortBinding>> {
        return portMappings.map { it.container.toString() to listOf(PortBinding.of("0.0.0.0", it.host)) }.toMap()
    }

    private fun containerConfig(hostConfig: HostConfig?): ContainerConfig? {
        return ContainerConfig.builder()
                .hostConfig(hostConfig)
                .env(env)
                .image("$image:$tag")
                .exposedPorts(*portMappings.map { it.container.toString() }.toTypedArray())
                .build()
    }

    private fun getDockerClient(): DockerClient {
        try {
            return DefaultDockerClient.fromEnv().build()
        } catch (e: Exception) {
            log.error("Couldn't connect to the Docker", e)
            throw e
        }
    }
}

/**
 * Data class encapsulating port mapping specifications:
 *  - [host] The port number we want the container to listen on the host OS.
 *  - [container] The port number inside the container.
 *
 * @author Mehran Behnam
 */
data class PortMapping(val host: Int, val container: Int) {

    override fun toString(): String {
        return "$host:$container"
    }
}

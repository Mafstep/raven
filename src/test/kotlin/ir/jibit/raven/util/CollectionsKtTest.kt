package ir.jibit.raven.util

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments.arguments
import org.junit.jupiter.params.provider.MethodSource

/**
 * Unit tests for collection utils.
 *
 * @author Mehran Behnam
 */
class CollectionsKtTest {

    @ParameterizedTest
    @MethodSource("provideCollections")
    fun `isCompletelyEmpty should return true for truly empty collections`(input: Collection<String>?, expected: Boolean) {
        assertThat(input.isCompletelyEmpty())
            .isEqualTo(expected)
    }

    companion object {

        @JvmStatic fun provideCollections() = listOf(
            arguments(listOf("", "   "), true),
            arguments(setOf("", "   "), true),
            arguments(setOf("", "   ", "te"), false),
            arguments(null, true),
            arguments(emptyList<String>(), true),
            arguments(emptySet<String>(), true)
        )
    }
}

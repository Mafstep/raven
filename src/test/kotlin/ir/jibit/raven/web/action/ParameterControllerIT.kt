package ir.jibit.raven.web.action

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.whenever
import ir.jibit.raven.body
import ir.jibit.raven.emptyBody
import ir.jibit.raven.model.action.Parameter
import ir.jibit.raven.model.action.ParameterType.NUMBER
import ir.jibit.raven.model.action.ParameterType.STRING
import ir.jibit.raven.service.action.ParameterAlreadyExistsException
import ir.jibit.raven.service.action.ParameterService
import ir.jibit.raven.web.BasicMvcTest
import ir.jibit.raven.web.parameters
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments.arguments
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.http.MediaType.APPLICATION_JSON_UTF8
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

/**
 * Integration tests for [ParameterController].
 *
 * @author Mehran Behnam
 */
@WebMvcTest(ParameterController::class)
class ParameterControllerIT : BasicMvcTest() {

    /**
     * Mocked version of [ParameterService].
     */
    @MockBean private lateinit var parameterService: ParameterService

    /*************************************************************************************
     * Tests for create parameter endpoint
     *************************************************************************************/

    @ParameterizedTest
    @MethodSource("paramsForCreateParameter")
    fun `POSTing invalid body to parameters endpoint would result in 400 Bad Request`(body: Any,
                                                                                      expectedErrors: Array<String>) {
        mvc.perform(post(parameters).contentType(APPLICATION_JSON_UTF8).content(toJson(body)))
                .andExpect(status().isBadRequest)
                .andExpect(hasErrors(*expectedErrors))
    }

    @Test
    fun `POSTing a duplicate parameter should result in a 400 Bad Request`() {
        whenever(parameterService.createParameter(any())).thenThrow(ParameterAlreadyExistsException("param"))

        val body = body("name" to "amount", "type" to "NUMBER")
        mvc.perform(post(parameters).contentType(APPLICATION_JSON_UTF8).content(toJson(body)))
                .andExpect(status().isBadRequest)
                .andExpect(hasErrors("parameter.already_exists"))
    }

    @Test
    fun `When everything is OK, POSTing to parameters endpoint should result in a 200 OK response`() {
        val body = body("name" to "amount", "type" to "NUMBER")
        mvc.perform(post(parameters).contentType(APPLICATION_JSON_UTF8).content(toJson(body)))
                .andExpect(status().isNoContent)
    }

    /*************************************************************************************
     * Tests for get parameter endpoint
     *************************************************************************************/

    @Test
    fun `When the requested parameter does not exist, it should return 404 Not Found`() {
        whenever(parameterService.getParameter(any())).thenReturn(null)

        mvc.perform(get("$parameters/invalid"))
                .andExpect(status().isNotFound)
                .andExpect(hasErrors("action.parameter.not_found"))
    }

    @Test
    fun `When the requested parameter does exist, it should return the parameter details`() {
        val parameter = Parameter("amount", NUMBER)
        whenever(parameterService.getParameter(any())).thenReturn(parameter)

        mvc.perform(get("$parameters/amount"))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.name").value("amount"))
                .andExpect(jsonPath("$.type").value("NUMBER"))
    }

    /*************************************************************************************
     * Tests for list parameters endpoint
     *************************************************************************************/

    @Test
    fun `The list parameters endpoint should return a paginated list of parameters`() {
        val params = listOf(Parameter("amount", NUMBER), Parameter("name", STRING))
        val page = PageImpl(params, PageRequest.of(0, 10), 10)
        whenever(parameterService.listParameters(any())).thenReturn(page)

        mvc.perform(get(parameters))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.page_number").value("1"))
                .andExpect(jsonPath("$.size").value("10"))
                .andExpect(jsonPath("$.elements[0].name").value("amount"))
                .andExpect(jsonPath("$.elements[0].type").value("NUMBER"))
                .andExpect(jsonPath("$.elements[1].name").value("name"))
                .andExpect(jsonPath("$.elements[1].type").value("STRING"))
    }

    /*************************************************************************************
     * Auxiliary Stuff
     *************************************************************************************/

    companion object {
        private const val requiredName = "parameter.name.required"
        private const val requiredType = "parameter.type.required"

        @JvmStatic
        fun paramsForCreateParameter() = listOf(
                arguments(emptyBody(), arrayOf(requiredName, requiredType)),
                arguments(body("name" to ""), arrayOf(requiredName, requiredType)),
                arguments(body("name" to "  "), arrayOf(requiredName, requiredType)),
                arguments(body("name" to "Name"), arrayOf(requiredType)),
                arguments(body("name" to "Name", "type" to ""), arrayOf(requiredType)),
                arguments(body("name" to "Name", "type" to "  "), arrayOf(requiredType)),
                arguments(body("name" to "Name", "type" to "invalid"), arrayOf(requiredType))
        )
    }
}

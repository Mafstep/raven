package ir.jibit.raven.web.action

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.argumentCaptor
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import ir.jibit.raven.body
import ir.jibit.raven.emptyBody
import ir.jibit.raven.model.action.Action
import ir.jibit.raven.model.action.ActionRule
import ir.jibit.raven.model.action.Parameter
import ir.jibit.raven.model.action.ParameterType.NUMBER
import ir.jibit.raven.model.notification.NotificationType.PUSH
import ir.jibit.raven.model.notification.NotificationType.SMS
import ir.jibit.raven.service.action.ActionAlreadyExistsException
import ir.jibit.raven.service.action.ActionService
import ir.jibit.raven.service.action.InvalidActionRuleFallbackReferenceException
import ir.jibit.raven.service.action.ParameterNotFoundException
import ir.jibit.raven.web.BasicMvcTest
import ir.jibit.raven.web.actions
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.Arguments.arguments
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.http.MediaType.APPLICATION_JSON_UTF8
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

/**
 * Integration tests for [ActionController].
 *
 * @author Mehran Behnam
 */
@WebMvcTest(ActionController::class)
class ActionControllerIT : BasicMvcTest() {

    /**
     * A mocked [ActionService] implementation.
     */
    @MockBean private lateinit var actionService: ActionService

    /*************************************************************************************
     * Tests for action creation endpoint
     *************************************************************************************/

    @ParameterizedTest
    @MethodSource("invalidBodyPostActions")
    fun `POSTing an invalid body should result in a 400 Bad Request with error codes`(body: Any,
                                                                                      errorCodes: Array<String>) {
        mvc.perform(post(actions).contentType(APPLICATION_JSON_UTF8).content(toJson(body)))
                .andExpect(status().isBadRequest)
                .andExpect(hasErrors(*errorCodes))
    }

    @Test
    fun `POSTing a valid action should result in a 204 No Content result`() {
        val action = validActionToCreate()

        mvc.perform(post(actions).contentType(APPLICATION_JSON_UTF8).content(toJson(action)))
                .andExpect(status().isNoContent)

        argumentCaptor<Action>().apply {
            verify(actionService).createAction(capture())

            val capturedAction = firstValue
            assertThat(capturedAction.name).isEqualTo("TopUp")
            assertThat(capturedAction.parameters).containsOnly(Parameter("amount", NUMBER))
            assertThat(capturedAction.actionRules).containsOnly(ActionRule(notificationType = PUSH))
        }
    }

    @Test
    fun `When the action exists already, a 400 Bad Request should return`() {
        val action = validActionToCreate()
        whenever(actionService.createAction(any())).thenThrow(ActionAlreadyExistsException(""))

        mvc.perform(post(actions).contentType(APPLICATION_JSON_UTF8).content(toJson(action)))
                .andExpect(status().isBadRequest)
                .andExpect(hasErrors("action.already_exists"))
    }

    @Test
    fun `When the action parameter is invalid, a 404 Not Found should return`() {
        val action = validActionToCreate()
        whenever(actionService.createAction(any())).thenThrow(ParameterNotFoundException(""))

        mvc.perform(post(actions).contentType(APPLICATION_JSON_UTF8).content(toJson(action)))
                .andExpect(status().isNotFound)
                .andExpect(hasErrors("action.parameter.not_found"))
    }

    @Test
    fun `When the action fallbacks are invalid, a 400 Bad Request should return`() {
        val action = validActionToCreate()
        whenever(actionService.createAction(any())).thenThrow(InvalidActionRuleFallbackReferenceException(setOf()))

        mvc.perform(post(actions).contentType(APPLICATION_JSON_UTF8).content(toJson(action)))
                .andExpect(status().isBadRequest)
                .andExpect(hasErrors("action.rule.invalid_fallback"))
    }

    private fun validActionToCreate() =
            body(
                    "name" to "TopUp",
                    "parameters" to listOf("amount"),
                    "action_rules" to listOf(mapOf("notification_type" to "PUSH"))
            )

    /*************************************************************************************
     * Tests for list actions endpoint
     *************************************************************************************/

    @Test
    fun `Should return a paginated list of actions`() {
        val action = Action("TopUp", setOf(Parameter("amount", NUMBER)), setOf(ActionRule(PUSH), ActionRule(SMS)))
        val page = PageImpl(listOf(action), PageRequest.of(0, 20), 1)
        whenever(actionService.listActions(any())).thenReturn(page)

        mvc.perform(get(actions))
                .andExpect(status().isOk)
                // Page number converted back to 1-indexed
                .andExpect(jsonPath("$.page_number").value(1))
                .andExpect(jsonPath("$.size").value(20))
                .andExpect(jsonPath("$.elements[0].name").value("TopUp"))
                .andExpect(jsonPath("$.elements[0].parameters[0].name").value("amount"))
                .andExpect(jsonPath("$.elements[0].parameters[0].type").value("NUMBER"))
                .andExpect(jsonPath("$.elements[0].action_rules[0].notification_type").value("PUSH"))
                .andExpect(jsonPath("$.elements[0].action_rules[1].notification_type").value("SMS"))
    }

    /*************************************************************************************
     * Tests for getting an action
     *************************************************************************************/

    @Test
    fun `When the requested action does not exist, it should return a 404 Not Found`() {
        whenever(actionService.getAction(any())).thenReturn(null)

        mvc.perform(get("$actions/TopUp"))
                .andExpect(status().isNotFound)
                .andExpect(hasErrors("action.not_found"))
    }

    @Test
    fun `If the requested action does exist, it should return the requested action details in a 200 OK response`() {
        val action = Action("TopUp", setOf(Parameter("amount", NUMBER)), setOf(ActionRule(PUSH)))
        whenever(actionService.getAction(any())).thenReturn(action)

        mvc.perform(get("$actions/TopUp"))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.name").value("TopUp"))
                .andExpect(jsonPath("$.parameters[0].name").value("amount"))
                .andExpect(jsonPath("$.parameters[0].type").value("NUMBER"))
                .andExpect(jsonPath("$.action_rules[0].notification_type").value("PUSH"))
                .andExpect(jsonPath("$.action_rules[0].fallback_for").doesNotExist())
    }

    /*************************************************************************************
     * Auxiliary Stuff
     *************************************************************************************/

    companion object {

        /**
         * Provides a few invalid parameters to test the validation when creating actions.
         */
        @JvmStatic
        fun invalidBodyPostActions(): List<Arguments> {
            val requiredName = "action.name.required"
            val requiredRules = "action.rules.required"
            val invalidParams = "action.parameters.invalid"
            val requiredNotification = "action.rules.notification.required"

            return listOf(
                    arguments(emptyBody(), arrayOf(requiredName, requiredRules)),
                    arguments(body("name" to ""), arrayOf(requiredName, requiredRules)),
                    arguments(body("name" to "  "), arrayOf(requiredName, requiredRules)),
                    arguments(body("name" to "TopUp"), arrayOf(requiredRules)),
                    arguments(body("name" to "TopUp", "action_rules" to null), arrayOf(requiredRules)),
                    arguments(body("name" to "TopUp", "action_rules" to emptyList<Any>()), arrayOf(requiredRules)),
                    arguments(
                            body("name" to "TopUp", "action_rules" to listOf(emptyMap<String, String>())),
                            arrayOf(requiredNotification)
                    ),
                    arguments(
                            body("name" to "TopUp", "action_rules" to listOf(mapOf("notification_type" to "invalid"))),
                            arrayOf(requiredNotification)
                    ),
                    arguments(
                            body(
                                    "name" to "TopUp",
                                    "action_rules" to listOf(mapOf("notification_type" to "push")),
                                    "parameters" to listOf("", "age")
                            ),
                            arrayOf(invalidParams)
                    ),
                    arguments(
                            body(
                                    "name" to "TopUp",
                                    "action_rules" to listOf(mapOf("notification_type" to "push")),
                                    "parameters" to listOf("  ", "age")
                            ),
                            arrayOf(invalidParams)
                    )
            )
        }
    }
}

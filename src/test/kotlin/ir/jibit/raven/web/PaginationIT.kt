package ir.jibit.raven.web

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.argumentCaptor
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import ir.jibit.raven.model.action.Action
import ir.jibit.raven.model.action.ActionRule
import ir.jibit.raven.model.action.Parameter
import ir.jibit.raven.model.action.ParameterType.NUMBER
import ir.jibit.raven.model.notification.NotificationType.PUSH
import ir.jibit.raven.model.notification.NotificationType.SMS
import ir.jibit.raven.service.action.ActionService
import ir.jibit.raven.web.action.ActionController
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

/**
 * Integration tests to test [Pageable] support in our web tier.
 *
 * @author Mehran Behnam
 */
@WebMvcTest(ActionController::class)
class PaginationIT : BasicMvcTest() {

    /**
     * A mocked [ActionService] implementation.
     */
    @MockBean private lateinit var actionService: ActionService

    @Test
    fun `Page numbers are indexed by one`() {
        returnDefaultActions()

        // When passing page numbers, they are 1-indexed.
        mvc.perform(get(actions).param("page", "2"))
                .andExpect(MockMvcResultMatchers.status().isOk)

        argumentCaptor<Pageable>().apply {
            verify(actionService).listActions(capture())

            // Then Spring would translate those 1-based page numbers to our be-loved zero-index ones.
            assertThat(firstValue.pageNumber).isEqualTo(1)
        }
    }

    @Test
    fun `We can't exceed the maximum possible page size`() {
        returnDefaultActions()

        // The maximum is 100
        mvc.perform(get(actions).param("size", "200"))

        argumentCaptor<Pageable>().apply {
            verify(actionService).listActions(capture())

            // Although we passed 200 as the page size, the Spring reduced it to 100.
            assertThat(firstValue.pageSize).isEqualTo(100)
        }
    }

    @Test
    fun `Invalid pagination parameters should be translated to default parameters`() {
        returnDefaultActions()

        mvc.perform(get(actions).param("page", "-1").param("size", "-5"))

        argumentCaptor<Pageable>().apply {
            verify(actionService).listActions(capture())

            assertThat(firstValue.pageNumber).isEqualTo(0)
            assertThat(firstValue.pageSize).isEqualTo(20)
        }
    }

    private fun returnDefaultActions() {
        val action = Action("TopUp", setOf(Parameter("amount", NUMBER)), setOf(ActionRule(PUSH), ActionRule(SMS)))
        val page = PageImpl(listOf(action), PageRequest.of(0, 20), 1)
        whenever(actionService.listActions(any())).thenReturn(page)
    }
}

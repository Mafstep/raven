package ir.jibit.raven.web.error

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import org.assertj.core.api.Assertions
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments.arguments
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.core.convert.ConversionFailedException
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException

/**
 * Unit tests for [RavenExceptionRefiner] exception refiner.
 *
 * @author Mehran Behnam
 */
class RavenExceptionRefinerTest {

    /**
     * Subject under test.
     */
    private val refiner = RavenExceptionRefiner()

    @ParameterizedTest
    @MethodSource("provideParams")
    fun `Refine should be able to transform the given exception properly`(input: Throwable?,
                                                                          expected: Class<Throwable?>?) {
        when (expected) {
            null -> Assertions.assertThat(refiner.refine(input)).isNull()
            else -> Assertions.assertThat(refiner.refine(input)).isInstanceOf(expected)
        }
    }

    companion object {
        @JvmStatic
        fun provideParams() = listOf(
                arguments(null, null),
                arguments(IllegalArgumentException(), IllegalArgumentException::class.java),
                arguments(conversionException(IllegalArgumentException()), IllegalArgumentException::class.java),
                arguments(conversionException(null), null),
                arguments(typeMismatchException(null), null),
                arguments(typeMismatchException(IllegalArgumentException()), IllegalArgumentException::class.java)
        )

        private fun conversionException(cause: Throwable?) = mock<ConversionFailedException> {
            whenever(it.cause).thenReturn(cause)
        }

        private fun typeMismatchException(cause: Throwable?) =
                MethodArgumentTypeMismatchException(null, null, "dummy", mock(), RuntimeException(cause))
    }
}

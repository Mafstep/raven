package ir.jibit.raven.web.notification

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.whenever
import ir.jibit.raven.body
import ir.jibit.raven.emptyBody
import ir.jibit.raven.model.notification.*
import ir.jibit.raven.model.token.RegistrationToken
import ir.jibit.raven.model.token.TokenType
import ir.jibit.raven.notification.NotificationProcessingFailureException
import ir.jibit.raven.service.notification.NotificationService
import ir.jibit.raven.service.token.RegistrationTokenService
import ir.jibit.raven.web.BasicMvcTest
import ir.jibit.raven.web.notifications
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments.arguments
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.SliceImpl
import org.springframework.http.MediaType.APPLICATION_JSON_UTF8
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.lang.RuntimeException
import java.time.Instant
import java.time.format.DateTimeFormatter
import java.util.*

/**
 * Integration tests for [NotificationController].
 *
 * @author Mehran Behnam
 */
@WebMvcTest(NotificationController::class)
class NotificationControllerIT : BasicMvcTest() {

    /**
     * Mocked version of [NotificationService].
     */
    @MockBean private lateinit var notificationService: NotificationService

    /**
     * Will be used to map user identifiers to device tokens.
     */
    @MockBean private lateinit var registrationTokenService: RegistrationTokenService


    /*************************************************************************************
     * Tests for submitting notifications
     *************************************************************************************/

    @ParameterizedTest
    @MethodSource("invalidParamsForSubmittingNotifications")
    fun `POSTing invalid params to notifications should return 400 Bad Request`(body: Any, expectedErrors: Array<String>) {
        mvc.perform(post(notifications).contentType(APPLICATION_JSON_UTF8).content(toJson(body)))
            .andExpect(status().isBadRequest)
            .andExpect(hasErrors(*expectedErrors))
    }

    @Test
    fun `When we failed to process a given notification, a 500 Internal Server Error should return with proper error code`() {
        val notification = body("type" to "SMS", "ttl" to 1000,
            "recipients" to listOf("+9893932452532"), "message" to "Hello World")

        val exception = NotificationProcessingFailureException(RuntimeException(), Notification())
        whenever(notificationService.sendNotification(any())).thenThrow(exception)

        mvc.perform(post(notifications).contentType(APPLICATION_JSON_UTF8).content(toJson(notification)))
            .andExpect(status().isInternalServerError)
            .andExpect(hasErrors("notification.failed_processing"))
    }

    @Test
    fun `If we couldn't find any corresponding token for the given user and there are no other destinations, a 400 Bad Request should return`() {
        val push = body("type" to "PUSH", "ttl" to "3600",
            "users" to setOf("123", "456"), "title" to "123", "body" to "123")

        whenever(registrationTokenService.getDeviceTokens(any())).thenReturn(emptySet())

        mvc.perform(post(notifications).contentType(APPLICATION_JSON_UTF8).content(toJson(push)))
            .andExpect(status().isBadRequest)
            .andExpect(hasErrors("notification.push.destination.required"))
    }

    @ParameterizedTest
    @MethodSource("validParamsForSubmittingNotifications")
    fun `POSTing valid parameters to notifications endpoint should result in a 200 OK response`(body: Any) {
        val token = RegistrationToken(user = "", tokenType = TokenType.ANDROID, token = "123", appVersion = "")
        whenever(registrationTokenService.getDeviceTokens(any())).thenReturn(setOf(token))

        mvc.perform(post(notifications).contentType(APPLICATION_JSON_UTF8).content(toJson(body)))
            .andExpect(status().isOk)
            .andExpect(jsonPath("$.id").exists())
            .andExpect(jsonPath("$.status").value("SUBMITTED"))
    }

    /*************************************************************************************
     * Tests for listing notifications
     *************************************************************************************/

    @Test
    fun `GETing the notifications should return a paginated list of notifications`() {
        val page = PageRequest.of(0, 3)
        val sms = SmsNotification(recipients = setOf("0912xxx"), message = "123")
        val push = PushNotification(topic = "", title = "", body = "")
        val email = EmailNotification(recipients = setOf("a@g.com"), body = "", subject = "")
        val allNotifications = listOf(sms, push, email)
        val result = SliceImpl(allNotifications, page, true)

        whenever(notificationService.latestNotifications(page)).thenReturn(result)

        mvc.perform(get(notifications).param("page", "1").param("size", "3"))
            .andExpect(status().is2xxSuccessful)

            // General pagination information
            .andExpect(jsonPath("$.page_number").value(1))
            .andExpect(jsonPath("$.size").value(3))
            .andExpect(jsonPath("$.number_of_elements").value(3))
            .andExpect(jsonPath("$.has_next").value("true"))
            .andExpect(jsonPath("$.has_previous").value("false"))

            // First one is SMS
            .andExpect(jsonPath("$.elements[0].id").value(sms.id.toString()))
            .andExpect(jsonPath("$.elements[0].status").value("SUBMITTED"))
            .andExpect(jsonPath("$.elements[0].when_submitted").value(formatInstant(sms.whenSubmitted)))
            .andExpect(jsonPath("$.elements[0].type").value("SMS"))

            // Second one is PUSH
            .andExpect(jsonPath("$.elements[1].id").value(push.id.toString()))
            .andExpect(jsonPath("$.elements[1].status").value("SUBMITTED"))
            .andExpect(jsonPath("$.elements[1].when_submitted").value(formatInstant(push.whenSubmitted)))
            .andExpect(jsonPath("$.elements[1].type").value("PUSH"))

            // Third one is PUSH
            .andExpect(jsonPath("$.elements[2].id").value(email.id.toString()))
            .andExpect(jsonPath("$.elements[2].status").value("SUBMITTED"))
            .andExpect(jsonPath("$.elements[2].when_submitted").value(formatInstant(email.whenSubmitted)))
            .andExpect(jsonPath("$.elements[2].type").value("EMAIL"))
    }

    /*************************************************************************************
     * Tests for getting one specific notification
     *************************************************************************************/

    @Test
    fun `When the given id is not a valid uuid, a 404 not found should return`() {
        mvc.perform(get("$notifications/invalid-uuid"))
            .andExpect(status().isNotFound)
            .andExpect(hasErrors("notification.not_found"))
    }

    @Test
    fun `When we couldn't find the requested notification, a 404 not found should return`() {
        whenever(notificationService.getNotification(any())).thenReturn(null)

        mvc.perform(get("$notifications/${UUID.randomUUID()}"))
            .andExpect(status().isNotFound)
            .andExpect(hasErrors("notification.not_found"))
    }

    @Test
    fun `If the requested notification is a push one, then we should return its details in a 200 OK body`() {
        val push = PushNotification("title for push", "body", setOf("token1", "token2"), "topic")
        push.data = mapOf("amount" to "1000", "phone_number" to "123")
        push.ttl = 1000
        push.notificationState = NotificationState.DONE
        push.processLog = "Successfully done!"

        whenever(notificationService.getNotification(push.id)).thenReturn(push)

        mvc.perform(get("$notifications/${push.id}"))
            .andExpect(status().isOk)
            .andExpect(jsonPath("$.id").value(push.id.toString()))
            .andExpect(jsonPath("$.ttl").value(push.ttl))
            .andExpect(jsonPath("$.status").value("DONE"))
            .andExpect(jsonPath("$.data.amount").value("1000"))
            .andExpect(jsonPath("$.data.phone_number").value("123"))
            .andExpect(jsonPath("$.process_log").value(push.processLog!!))
            .andExpect(jsonPath("$.when_submitted").value(formatInstant(push.whenSubmitted)))
            .andExpect(jsonPath("$.type").value("PUSH"))
            .andExpect(jsonPath("$.topic").value("topic"))
            .andExpect(jsonPath("$.title").value("title for push"))
            .andExpect(jsonPath("$.body").value("body"))
            .andExpect(jsonPath("$.recipient_tokens[0]").value("token1"))
            .andExpect(jsonPath("$.recipient_tokens[1]").value("token2"))
            .andExpect(jsonPath("$.subject").doesNotExist())
            .andExpect(jsonPath("$.recipients").doesNotExist())
            .andExpect(jsonPath("$.message").doesNotExist())
    }

    @Test
    fun `If the requested notification is a sms one, then we should return its details in a 200 OK body`() {
        val sms = SmsNotification(recipients = setOf("0912xx", "0939xx"), message = "hi")
        sms.data = mapOf("amount" to "1000", "phone_number" to "123")
        sms.ttl = 1000
        sms.notificationState = NotificationState.PROCESSING
        sms.processLog = "Successfully done!"

        whenever(notificationService.getNotification(sms.id)).thenReturn(sms)

        mvc.perform(get("$notifications/${sms.id}"))
            .andExpect(status().isOk)
            .andExpect(jsonPath("$.id").value(sms.id.toString()))
            .andExpect(jsonPath("$.ttl").value(sms.ttl))
            .andExpect(jsonPath("$.status").value("PROCESSING"))
            .andExpect(jsonPath("$.data.amount").value("1000"))
            .andExpect(jsonPath("$.data.phone_number").value("123"))
            .andExpect(jsonPath("$.process_log").value(sms.processLog!!))
            .andExpect(jsonPath("$.when_submitted").value(formatInstant(sms.whenSubmitted)))
            .andExpect(jsonPath("$.type").value("SMS"))
            .andExpect(jsonPath("$.topic").doesNotExist())
            .andExpect(jsonPath("$.title").doesNotExist())
            .andExpect(jsonPath("$.body").doesNotExist())
            .andExpect(jsonPath("$.recipient_tokens").doesNotExist())
            .andExpect(jsonPath("$.subject").doesNotExist())
            .andExpect(jsonPath("$.recipients[0]").value("0912xx"))
            .andExpect(jsonPath("$.recipients[1]").value("0939xx"))
            .andExpect(jsonPath("$.message").value("hi"))
    }

    @Test
    fun `If the requested notification is an email one, then we should return its details in a 200 OK body`() {
        val email = EmailNotification(recipients = setOf("a@g.com"), subject = "hi", body = "hi")
        email.ttl = 1000
        email.notificationState = NotificationState.FAILED
        email.processLog = "Successfully done!"

        whenever(notificationService.getNotification(email.id)).thenReturn(email)

        mvc.perform(get("$notifications/${email.id}"))
            .andExpect(status().isOk)
            .andExpect(jsonPath("$.id").value(email.id.toString()))
            .andExpect(jsonPath("$.ttl").value(email.ttl))
            .andExpect(jsonPath("$.status").value("FAILED"))
            .andExpect(jsonPath("$.data").isEmpty)
            .andExpect(jsonPath("$.process_log").value(email.processLog!!))
            .andExpect(jsonPath("$.when_submitted").value(formatInstant(email.whenSubmitted)))
            .andExpect(jsonPath("$.type").value("EMAIL"))
            .andExpect(jsonPath("$.topic").doesNotExist())
            .andExpect(jsonPath("$.title").doesNotExist())
            .andExpect(jsonPath("$.body").value("hi"))
            .andExpect(jsonPath("$.recipient_tokens").doesNotExist())
            .andExpect(jsonPath("$.subject").value("hi"))
            .andExpect(jsonPath("$.recipients[0]").value("a@g.com"))
            .andExpect(jsonPath("$.message").doesNotExist())
    }

    /*************************************************************************************
     * Auxiliary Stuff
     *************************************************************************************/

    private fun formatInstant(instant: Instant) = DateTimeFormatter.ISO_INSTANT.format(instant)

    companion object {
        private const val typeRequired = "notification.type.required"
        private const val ttlRequired = "notification.ttl.required"
        private const val typeInvalid = "notification.type.invalid"
        private const val pushDestinationRequired = "notification.push.destination.required"
        private const val pushTitleRequired = "notification.push.title.required"
        private const val pushBodyRequired = "notification.push.body.required"
        private const val smsRecipientRequired = "notification.sms.recipients.required"
        private const val smsMessageRequired = "notification.sms.message.required"
        private const val smsRecipientInvalid = "notification.sms.recipients.invalid"
        private const val emailRecipientsRequired = "notification.email.recipients.required"
        private const val emailRecipientsInvalid = "notification.email.recipients.invalid"
        private const val emailSubjectRequired = "notification.email.subject.required"
        private const val emailBodyRequired = "notification.email.body.required"

        /**
         * Provides invalid parameters for POST /notifications.
         */
        @JvmStatic
        fun invalidParamsForSubmittingNotifications() = listOf(

            // General validations
            arguments(emptyBody(), arrayOf(typeRequired, ttlRequired)),
            arguments(body("type" to ""), arrayOf(typeRequired, ttlRequired)),
            arguments(body("type" to "  "), arrayOf(typeRequired, ttlRequired)),
            arguments(body("type" to "invalid"), arrayOf(ttlRequired)),
            arguments(body("type" to "invalid", "ttl" to 100), arrayOf(typeInvalid)),
            arguments(body("type" to "PUSH"), arrayOf(ttlRequired, pushBodyRequired, pushTitleRequired)),

            // Push specific validations
            arguments(body("type" to "PUSH", "ttl" to "3600"), arrayOf(pushTitleRequired, pushBodyRequired)),
            arguments(body("type" to "PUSH", "ttl" to "3600", "title" to ""), arrayOf(pushTitleRequired, pushBodyRequired)),
            arguments(body("type" to "PUSH", "ttl" to "3600", "title" to "  "), arrayOf(pushTitleRequired, pushBodyRequired)),
            arguments(body("type" to "PUSH", "ttl" to "3600", "title" to "123"), arrayOf(pushBodyRequired)),
            arguments(body("type" to "PUSH", "ttl" to "3600", "title" to "123", "body" to ""), arrayOf(pushBodyRequired)),
            arguments(body("type" to "PUSH", "ttl" to "3600", "title" to "123", "body" to "  "), arrayOf(pushBodyRequired)),
            arguments(body("type" to "PUSH", "ttl" to "3600", "title" to "123", "body" to "123"), arrayOf(pushDestinationRequired)),
            arguments(body("type" to "PUSH", "ttl" to "3600", "topic" to "", "title" to "123", "body" to "123"), arrayOf(pushDestinationRequired)),
            arguments(body("type" to "PUSH", "ttl" to "3600", "topic" to "  ", "title" to "123", "body" to "123"), arrayOf(pushDestinationRequired)),
            arguments(body("type" to "PUSH", "ttl" to "3600", "recipient_tokens" to emptyList<String>(), "title" to "123", "body" to "123"), arrayOf(pushDestinationRequired)),
            arguments(body("type" to "PUSH", "ttl" to "3600", "recipient_tokens" to listOf("", "  "), "title" to "123", "body" to "123"), arrayOf(pushDestinationRequired)),
            arguments(body("type" to "PUSH", "ttl" to "3600", "users" to emptyList<String>(), "title" to "123", "body" to "123"), arrayOf(pushDestinationRequired)),
            arguments(body("type" to "PUSH", "ttl" to "3600", "users" to listOf("", "   ", ""), "title" to "123", "body" to "123"), arrayOf(pushDestinationRequired)),

            // SMS specific validations
            arguments(body("type" to "SMS", "ttl" to "3600"), arrayOf(smsMessageRequired, smsRecipientRequired)),
            arguments(body("type" to "SMS", "ttl" to "3600", "message" to ""), arrayOf(smsMessageRequired, smsRecipientRequired)),
            arguments(body("type" to "SMS", "ttl" to "3600", "message" to "  "), arrayOf(smsMessageRequired, smsRecipientRequired)),
            arguments(body("type" to "SMS", "ttl" to "3600", "message" to "Hello"), arrayOf(smsRecipientRequired)),
            arguments(body("type" to "SMS", "ttl" to "3600", "message" to "Hello", "recipients" to emptyList<String>()), arrayOf(smsRecipientRequired)),
            arguments(body("type" to "SMS", "ttl" to "3600", "message" to "Hello", "recipients" to listOf("abc")), arrayOf(smsRecipientInvalid)),
            arguments(body("type" to "SMS", "ttl" to "3600", "message" to "Hello", "recipients" to listOf("  ", "")), arrayOf(smsRecipientInvalid)),
            arguments(body("type" to "SMS", "ttl" to "3600", "message" to "Hello", "recipients" to listOf("123", "")), arrayOf(smsRecipientInvalid)),
            arguments(body("type" to "SMS", "ttl" to "3600", "message" to "Hello", "recipients" to listOf("123", "abc")), arrayOf(smsRecipientInvalid)),
            arguments(body("type" to "SMS", "ttl" to "3600", "message" to "Hello", "recipients" to listOf("123", "++98123")), arrayOf(smsRecipientInvalid)),

            // Email specific validations
            arguments(body("type" to "EMAIL", "ttl" to "3600"), arrayOf(emailBodyRequired, emailRecipientsRequired, emailSubjectRequired)),
            arguments(body("type" to "EMAIL", "ttl" to "3600", "body" to ""), arrayOf(emailBodyRequired, emailRecipientsRequired, emailSubjectRequired)),
            arguments(body("type" to "EMAIL", "ttl" to "3600", "body" to "   "), arrayOf(emailBodyRequired, emailRecipientsRequired, emailSubjectRequired)),
            arguments(body("type" to "EMAIL", "ttl" to "3600", "body" to "Hello"), arrayOf(emailRecipientsRequired, emailSubjectRequired)),
            arguments(body("type" to "EMAIL", "ttl" to "3600", "body" to "Hello", "subject" to ""), arrayOf(emailRecipientsRequired, emailSubjectRequired)),
            arguments(body("type" to "EMAIL", "ttl" to "3600", "body" to "Hello", "subject" to "  "), arrayOf(emailRecipientsRequired, emailSubjectRequired)),
            arguments(body("type" to "EMAIL", "ttl" to "3600", "body" to "Hello", "subject" to "Greeting"), arrayOf(emailRecipientsRequired)),
            arguments(body("type" to "EMAIL", "ttl" to "3600", "body" to "Hello", "subject" to "Greeting", "recipients" to emptyList<String>()), arrayOf(emailRecipientsRequired)),
            arguments(body("type" to "EMAIL", "ttl" to "3600", "body" to "Hello", "subject" to "Greeting", "recipients" to listOf("")), arrayOf(emailRecipientsInvalid)),
            arguments(body("type" to "EMAIL", "ttl" to "3600", "body" to "Hello", "subject" to "Greeting", "recipients" to listOf("  ")), arrayOf(emailRecipientsInvalid)),
            arguments(body("type" to "EMAIL", "ttl" to "3600", "body" to "Hello", "subject" to "Greeting", "recipients" to listOf("a")), arrayOf(emailRecipientsInvalid)),
            arguments(body("type" to "EMAIL", "ttl" to "3600", "body" to "Hello", "subject" to "Greeting", "recipients" to listOf("a@g")), arrayOf(emailRecipientsInvalid)),
            arguments(body("type" to "EMAIL", "ttl" to "3600", "body" to "Hello", "subject" to "Greeting", "recipients" to listOf("a@g,com")), arrayOf(emailRecipientsInvalid)),
            arguments(body("type" to "EMAIL", "ttl" to "3600", "body" to "Hello", "subject" to "Greeting", "recipients" to listOf("a@g,com", "a@g.com")), arrayOf(emailRecipientsInvalid))
        )

        /**
         * Provides valid parameters for POST /notifications.
         */
        @JvmStatic
        fun validParamsForSubmittingNotifications() = listOf(

            // Push specific
            arguments(body("type" to "PUSH", "ttl" to 1000, "title" to "123", "body" to "123", "data" to mapOf("amount" to 1000), "topic" to "/topics/updates")),
            arguments(body("type" to "PUSH", "ttl" to 1000, "title" to "123", "body" to "123", "recipient_tokens" to listOf("132", "123"))),
            arguments(body("type" to "PUSH", "ttl" to 1000, "title" to "123", "body" to "123", "recipient_tokens" to listOf("132"), "data" to mapOf("action" to "TopUp"))),
            arguments(body("type" to "PUSH", "ttl" to 1000, "title" to "123", "body" to "123", "users" to listOf("132"), "data" to mapOf("action" to "TopUp"))),

            // Email specific
            arguments(body("type" to "EMAIL", "ttl" to 1000, "data" to mapOf("amount" to 1000), "body" to "Hello", "subject" to "Howdy", "recipients" to listOf("a@g.com"))),
            arguments(body("type" to "EMAIL", "ttl" to 1000, "data" to mapOf("amount" to 1000), "body" to "Hello", "subject" to "Howdy", "recipients" to listOf("a_dg@g.com"))),
            arguments(body("type" to "EMAIL", "ttl" to 1000, "data" to mapOf("amount" to 1000), "body" to "Hello", "subject" to "Howdy", "recipients" to listOf("a_dg@g.com", "1989.ad.gc_e@g.gov"))),

            // SMS specific
            arguments(body("type" to "SMS", "ttl" to 42, "message" to "Hi", "recipients" to listOf("09123", "+98123", "+123"))),
            arguments(body("type" to "SMS", "ttl" to 42, "message" to "Hi", "recipients" to listOf("+123")))
        )
    }
}

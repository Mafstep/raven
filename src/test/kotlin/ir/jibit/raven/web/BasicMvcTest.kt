package ir.jibit.raven.web

import com.fasterxml.jackson.databind.ObjectMapper
import ir.jibit.raven.conf.security.SecurityConfig
import ir.jibit.raven.web.BasicMvcTest.BasicTestMvcConfig
import ir.jibit.raven.web.error.RavenExceptionRefiner
import me.alidg.errors.annotation.AutoConfigureErrors
import org.hamcrest.Matchers.containsInAnyOrder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Import
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath

/**
 * Base class for all [org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest]s.
 *
 * @author Mehran Behnam
 */
@AutoConfigureErrors
@ActiveProfiles("test")
@Import(BasicTestMvcConfig::class)
open class BasicMvcTest {

    /**
     * The auto-configured [MockMvc] instance.
     *
     * [Server-Side Tests](https://bit.ly/2trl2fR)
     */
    @Autowired protected lateinit var mvc: MockMvc

    /**
     * Will be used for JSON serialization/de-serialization.
     */
    @Autowired protected lateinit var json: ObjectMapper

    /**
     * Checks if the given [errorCodes] are present in the response. The ordering
     * of errors do NOT effect the result of this query.
     *
     * [JsonPath](https://github.com/jayway/JsonPath)
     *
     * @param errorCodes The error codes to check
     * @return A matcher instance
     */
    protected fun hasErrors(vararg errorCodes: String) =
            jsonPath("$.errors[*].code", containsInAnyOrder(*errorCodes))

    /**
     * Converts the given [data] input to its JSON representation.
     *
     * @param data The data to convert
     * @return The converted JSON string
     */
    protected fun toJson(data: Any): String = try { json.writeValueAsString(data) } catch (e: Throwable) { "{}" }

    /**
     * A test configuration to setup the test environment. This class would:
     *  - Enable our error handling infrastructure
     *  - Registers the [RavenExceptionRefiner] to refine some exceptions before handling them.
     */
    @TestConfiguration
    @Import(SecurityConfig::class)
    @ComponentScan(basePackageClasses = [RavenExceptionRefiner::class])
    class BasicTestMvcConfig
}

package ir.jibit.raven.web.token

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.whenever
import ir.jibit.raven.body
import ir.jibit.raven.conf.security.SecurityConfig
import ir.jibit.raven.emptyBody
import ir.jibit.raven.model.user.User
import ir.jibit.raven.service.token.RegistrationTokenService
import ir.jibit.raven.service.user.UserService
import ir.jibit.raven.web.BasicMvcTest
import ir.jibit.raven.web.tokens
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments.arguments
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.context.annotation.ComponentScan
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

/**
 * Integration tests for [RegistrationTokenController].
 *
 * @author Nazila Akbari
 */
@MockBean(RegistrationTokenService::class)
@WebMvcTest(RegistrationTokenController::class)
class RegistrationTokenControllerIT : BasicMvcTest() {

    /**
     * Mocked version of [UserService].
     */
    @MockBean private lateinit var userService: UserService

    /*************************************************************************************
     * Tests for registering client token.
     *************************************************************************************/

    @ParameterizedTest
    @MethodSource("invalidParamsForRegisteringToken")
    fun `POSTing invalid params to the tokens API should return 400 Bad Request`(body: Any, expectedErrors: Array<String>) {
        whenever(userService.findByToken(any())).thenReturn(User("09377634951"))

        mvc.perform(post(tokens).contentType(MediaType.APPLICATION_JSON_UTF8).content(toJson(body))
            .header("Authorization", "Bearer token"))
            .andExpect(status().isBadRequest)
            .andExpect(hasErrors(*expectedErrors))
    }

    @Test
    fun `Sending a request without an authorization header should result in a 403 Forbidden`() {
        mvc.perform(post(tokens).contentType(MediaType.APPLICATION_JSON_UTF8).content(toJson(aValidToken())))
            .andExpect(status().isForbidden)
    }

    @Test
    fun `Sending a request with an invalid authorization format should result in a 403 Forbidden response`() {
        mvc.perform(post(tokens).contentType(MediaType.APPLICATION_JSON_UTF8).content(toJson(aValidToken()))
            .header("Authorization", "token"))
            .andExpect(status().isForbidden)
    }

    @Test
    fun `POSTing invalid bearer tokens should result in a 403 forbidden response`() {
        whenever(userService.findByToken("Bearer token")).thenReturn(User("09377634951"))

        mvc.perform(post(tokens).contentType(MediaType.APPLICATION_JSON_UTF8).content(toJson(aValidToken()))
            .header("Authorization", "Bearer other_token"))
            .andExpect(status().isForbidden)
    }

    @Test
    fun `POSTing valid parameters to tokens endpoint should result in a 204 OK response`() {
        whenever(userService.findByToken(any())).thenReturn(User("09377634951"))
        whenever(userService.currentAuthenticatedUser()).thenReturn(User("09377634951"))

        mvc.perform(post(tokens).contentType(MediaType.APPLICATION_JSON_UTF8).content(toJson(aValidToken()))
            .header("Authorization", "Bearer token"))
            .andExpect(status().isNoContent)
    }

    private fun aValidToken() = body("token" to "dkjdfije", "app_version" to "1.0.0", "token_type" to "ANDROID")

    companion object {

        // All possible error codes.
        private const val tokenRequired = "token.required"
        private const val tokenTypeRequired = "token.type.required"
        private const val appVersionRequired = "app.version.required"
        private const val tokenTypeInvalid = "token.type.invalid"

        /**
         * Provides invalid parameters for POST /tokens.
         */
        @JvmStatic
        fun invalidParamsForRegisteringToken() = listOf(
            arguments(emptyBody(), arrayOf(tokenRequired, tokenTypeRequired, appVersionRequired)),
            arguments(body("token" to ""), arrayOf(tokenRequired, tokenTypeRequired, appVersionRequired)),
            arguments(body("token" to "   "), arrayOf(tokenRequired, tokenTypeRequired, appVersionRequired)),
            arguments(body("token" to "cjksandan"), arrayOf(tokenTypeRequired, appVersionRequired)),
            arguments(body("token" to "cjksandan", "token_type" to ""), arrayOf(tokenTypeRequired, appVersionRequired)),
            arguments(body("token" to "cjksandan", "token_type" to "  "), arrayOf(tokenTypeRequired, appVersionRequired)),
            arguments(body("token" to "cjksandan", "token_type" to "IOS"), arrayOf(appVersionRequired)),
            arguments(body("token" to "cjksandan", "token_type" to "IOS", "app_version" to ""), arrayOf(appVersionRequired)),
            arguments(body("token" to "cjksandan", "token_type" to "IOS", "app_version" to "  "), arrayOf(appVersionRequired)),
            arguments(body("token_type" to "ANDROID"), arrayOf(tokenRequired, appVersionRequired)),
            arguments(body("app_version" to "1.0.0"), arrayOf(tokenTypeRequired, tokenRequired)),
            arguments(body("token" to "cjksandan", "token_type" to "ANDROID"), arrayOf(appVersionRequired)),
            arguments(body("token" to "cjksandan", "app_version" to "1.0.0"), arrayOf(tokenTypeRequired)),
            arguments(body("app_version" to "1.0.0", "token_type" to "ANDROID"), arrayOf(tokenRequired)),
            arguments(body("app_version" to "1.0.0", "token_type" to "ANDROID"), arrayOf(tokenRequired)),
            arguments(body("token" to "dkjdfije", "app_version" to "1.0.0", "token_type" to "TEST"), arrayOf(tokenTypeInvalid))
        )
    }

    /**
     * A simple test configuration just to pick up our security configurations.
     */
    @TestConfiguration
    @ComponentScan(basePackageClasses = [SecurityConfig::class])
    protected class ActionServiceTestConfig
}

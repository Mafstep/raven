package ir.jibit.raven

import java.sql.DriverManager

/**
 * A JUnit extension to start a MySQL server before all tests and tear it down after
 * running those tests. This extension will be blocked until the MySQL is ready
 * to accept connections.
 *
 * [JUnit 5: Extension Model](https://junit.org/junit5/docs/current/user-guide/#extensions)
 *
 * @author Mehran Behnam
 */
class MySqlServerExtension : DockerContainer(
        "mysql",
        "5.5",
        listOf("MYSQL_ROOT_PASSWORD=test", "MYSQL_DATABASE=test"),
        { waitForMySqlToStart() },
        PortMapping(3306, 3306)
)

private fun waitForMySqlToStart() {
    while (true) {
        try {
            val hostname = System.getenv("MYSQL_TEST_HOST") ?: "localhost"
            val url = "jdbc:mysql://$hostname:3306/test"
            val username = "root"
            val password = "test"
            DriverManager.getConnection(url, username, password).use {}
            break
        } catch (ignored: Throwable) {}
    }
}

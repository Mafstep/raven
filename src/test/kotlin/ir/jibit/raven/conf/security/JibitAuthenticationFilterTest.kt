package ir.jibit.raven.conf.security

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import org.springframework.http.HttpHeaders.AUTHORIZATION
import org.springframework.security.core.context.SecurityContextHolder
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Unit tests for [JibitAuthenticationFilter] servlet filter.
 *
 * @author Mehran Behnam
 */
class JibitAuthenticationFilterTest {

    /**
     * Subject under test.
     */
    private val filter = JibitAuthenticationFilter()

    /**
     * Dummy filter chain.
     */
    private val chain = mock<FilterChain>()

    /**
     * Dummy http response.
     */
    private val response = mock<HttpServletResponse>()

    /**
     * The mocked http request.
     */
    private lateinit var request: HttpServletRequest

    @BeforeEach
    fun `Create mock request before each test`() {
        request = mock()
    }

    @AfterEach
    fun `Clearing the security context holder after each test`() {
        SecurityContextHolder.clearContext()
    }

    @Test
    fun `When the authorization token is missing, then security context should be empty`() {
        filter.doFilter(request, response, chain)

        assertThat(SecurityContextHolder.getContext().authentication).isNull()
    }

    @ParameterizedTest
    @ValueSource(strings = ["123", "bearer", "Bearer", "", "   "])
    fun `When the authorization token is not present as expected, then security context should be empty`(token: String) {
        whenever(request.getHeader(AUTHORIZATION)).thenReturn(token)

        filter.doFilter(request, response, chain)

        assertThat(SecurityContextHolder.getContext().authentication).isNull()
    }

    @Test
    fun `When the token is present as expected, security context should contain the auth request`() {
        whenever(request.getHeader(AUTHORIZATION)).thenReturn("Bearer token")

        filter.doFilter(request, response, chain)

        val authentication = SecurityContextHolder.getContext().authentication
        assertThat(authentication).isNotNull
        assertThat(authentication).isInstanceOf(AuthenticationRequest::class.java)
        assertThat((authentication as AuthenticationRequest).credentials).isEqualTo("Bearer token")
    }
}

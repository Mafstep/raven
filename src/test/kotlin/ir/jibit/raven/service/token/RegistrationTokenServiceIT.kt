package ir.jibit.raven.service.token

import ir.jibit.raven.MySqlServerExtension
import ir.jibit.raven.model.token.RegistrationToken
import ir.jibit.raven.model.token.TokenType.*
import ir.jibit.raven.repository.RegistrationTokenRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.ComponentScan
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ActiveProfiles

/**
 * Integration tests for [RegistrationTokenService] service.
 *
 * @author Nazila Akbari
 * @author Mehran Behnam
 */
@DataJpaTest
@DirtiesContext
@ActiveProfiles("test")
@ExtendWith(MySqlServerExtension::class)
@AutoConfigureTestDatabase(replace = NONE)
class RegistrationTokenServiceIT {

    /**
     * Subject under test.
     */
    @Autowired private lateinit var registrationTokenService: RegistrationTokenService

    /**
     * Will be used to manage registrationToken entities.
     */
    @Autowired private lateinit var registrationTokenRepository: RegistrationTokenRepository

    @BeforeEach
    fun `Flushing the DB before each test`() {
        registrationTokenRepository.deleteAll()
    }

    /*************************************************************************************
     * Tests for registering a push token
     *************************************************************************************/

    @Test
    fun `registrationToken should successfully persist a registration token`() {
        val token = RegistrationToken(
            token = "token",
            tokenType = WEB,
            appVersion = "1.2.3",
            user = "123",
            metadata = mapOf("os" to "macOs")
        )

        val registeredToken = registrationTokenService.registerToken(token)

        assertThat(registeredToken.id).isEqualTo(token.id)
        assertThat(registeredToken.token).isEqualTo("token")
        assertThat(registeredToken.tokenType).isEqualTo(WEB)
        assertThat(registeredToken.appVersion).isEqualTo("1.2.3")
        assertThat(registeredToken.metadata).containsAllEntriesOf(mapOf("os" to "macOs"))
    }

    @Test
    fun `getDeviceTokens must return all device token of users`() {
        val token1 = RegistrationToken(token = "token 1", appVersion = "1.2.3", tokenType = ANDROID, user = "123")
        val token2 = RegistrationToken(token = "token 2", appVersion = "1.5.3", tokenType = WEB, user = "123")
        val token3 = RegistrationToken(token = "token 3", appVersion = "1.3.3", tokenType = IOS, user = "456")
        val token4 = RegistrationToken(token = "token 4", appVersion = "1.3.1", tokenType = ANDROID, user = "456")
        registrationTokenRepository.saveAll(setOf(token1, token2, token3, token4))

        val deviceTokens = registrationTokenService.getDeviceTokens(setOf("123", "456", "789"))
        assertThat(deviceTokens.size).isEqualTo(4)
        assertThat(deviceTokens.containsAll(setOf(token1, token2, token3, token4)))
    }

    /**
     * A simple test configuration to register the followings into our test context:
     *  - The default implementation of [RegistrationTokenService].
     */
    @TestConfiguration
    @ComponentScan(basePackageClasses = [RegistrationTokenService::class])
    protected class ActionServiceTestConfig
}

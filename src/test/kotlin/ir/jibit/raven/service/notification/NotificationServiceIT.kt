package ir.jibit.raven.service.notification

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.readValue
import ir.jibit.raven.AutoConfigureRabbit
import ir.jibit.raven.MySqlServerExtension
import ir.jibit.raven.conf.messaging.MessagingConfig
import ir.jibit.raven.conf.messaging.MessagingConfig.Companion.emailQueueName
import ir.jibit.raven.conf.messaging.MessagingConfig.Companion.pushQueueName
import ir.jibit.raven.conf.messaging.MessagingConfig.Companion.smsQueueName
import ir.jibit.raven.conf.notification.NotificationConfig
import ir.jibit.raven.model.notification.EmailNotification
import ir.jibit.raven.model.notification.PushNotification
import ir.jibit.raven.model.notification.SmsNotification
import ir.jibit.raven.repository.NotificationRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.amqp.rabbit.core.RabbitAdmin
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Import
import org.springframework.data.domain.PageRequest
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ActiveProfiles
import java.util.*

/**
 * Integration tests for [NotificationService] service implementation.
 *
 * @author Mehran Behnam
 * */
@DataJpaTest
@DirtiesContext
@AutoConfigureRabbit
@ActiveProfiles("test")
@ExtendWith(MySqlServerExtension::class)
@AutoConfigureTestDatabase(replace = NONE)
class NotificationServiceIT {

    /**
     * Subject under test.
     */
    @Autowired
    private lateinit var notificationService: NotificationService

    /**
     * Provides data access operations over notifications.
     */
    @Autowired
    private lateinit var notificationRepository: NotificationRepository

    /**
     * Will be used to interact with RabbitMQ server.
     */
    @Autowired
    private lateinit var rabbitTemplate: RabbitTemplate

    /**
     * Will be used to purge a RabbitMQ queue.
     */
    @Autowired
    private lateinit var rabbitAdmin: RabbitAdmin

    /**
     * Will be used for JSON serialization/de-serialization.
     */
    private val json = ObjectMapper().registerModule(JavaTimeModule())

    @BeforeEach
    fun `Clears the DB and flushes all queues`() {
        setOf(emailQueueName, pushQueueName, smsQueueName).forEach { rabbitAdmin.purgeQueue(it) }
        notificationRepository.deleteAll()
    }

    /*************************************************************************************
     * Tests for sending a notification
     *************************************************************************************/

    @Test
    fun `Push notifications should get published into the dedicated push queue`() {
        val notification = PushNotification(topic = "/topics/updates", title = "Title", body = "Body")
        notification.ttl = 3
        notification.data = mapOf("amount" to "10000")

        notificationService.sendNotification(notification)

        // Message should get published into the appropriate queue
        val message = rabbitTemplate.receive(pushQueueName, 300)
        val published = json.readValue<PushNotification>(message!!.body)
        assertThat(published.id).isEqualTo(notification.id)
        assertThat(published.topic).isEqualTo(notification.topic)
        assertThat(published.ttl).isEqualTo(3)
        assertThat(published.data).isEqualTo(mapOf("amount" to "10000"))

        // Message should get persisted
        val persisted = notificationRepository.findById(notification.id).get() as PushNotification
        assertThat(persisted.id).isEqualTo(notification.id)
        assertThat(persisted.topic).isEqualTo(notification.topic)
        assertThat(persisted.ttl).isEqualTo(3)
        assertThat(persisted.data).isEqualTo(mapOf("amount" to "10000"))
    }

    @Test
    fun `SMS notifications should get published into the dedicated SMS queue`() {
        val notification = SmsNotification(recipients = setOf("0912 xxx"), message = "Hello")
        notification.ttl = 5

        notificationService.sendNotification(notification)

        // Message should get published into the appropriate queue
        val message = rabbitTemplate.receive(smsQueueName, 300)
        val published = json.readValue<SmsNotification>(message!!.body)
        assertThat(published.id).isEqualTo(notification.id)
        assertThat(published.message).isEqualTo("Hello")
        assertThat(published.ttl).isEqualTo(5)
        assertThat(published.recipients).containsOnly("0912 xxx")

        // Message should get persisted
        val persisted = notificationRepository.findById(notification.id).get() as SmsNotification
        assertThat(persisted.id).isEqualTo(notification.id)
        assertThat(persisted.message).isEqualTo("Hello")
        assertThat(persisted.ttl).isEqualTo(5)
        assertThat(persisted.recipients).containsOnly("0912 xxx")
    }

    @Test
    fun `Email notifications should get published into the dedicated Email queue`() {
        val notification = EmailNotification(recipients = setOf("ali@gmail.com"), body = "Dear...", subject = "Hello")

        notificationService.sendNotification(notification)

        // Message should get published into the appropriate queue
        val message = rabbitTemplate.receive(emailQueueName, 300)
        val published = json.readValue<EmailNotification>(message!!.body)
        assertThat(published.id).isEqualTo(notification.id)
        assertThat(published.recipients).containsOnly("ali@gmail.com")
        assertThat(published.body).isEqualTo("Dear...")
        assertThat(published.subject).isEqualTo("Hello")

        // Message should get persisted
        val persisted = notificationRepository.findById(notification.id).get() as EmailNotification
        assertThat(persisted.id).isEqualTo(notification.id)
        assertThat(persisted.recipients).containsOnly("ali@gmail.com")
        assertThat(persisted.body).isEqualTo("Dear...")
        assertThat(persisted.subject).isEqualTo("Hello")
    }

    /*************************************************************************************
     * Tests for getting a notification
     *************************************************************************************/

    @Test
    fun `When the requested notification does not exist, it should return null`() {
        assertThat(notificationService.getNotification(UUID.randomUUID()))
            .isNull()
    }

    @Test
    fun `When the requested notification exists, it should return it as expected`() {
        val notification = PushNotification("Title", "Body", topic = "/topics/updates")
        notificationRepository.save(notification)

        val retrieved = notificationService.getNotification(notification.id)
        assertThat(retrieved).isNotNull
        assertThat(retrieved!!.id).isEqualTo(notification.id)
    }

    /*************************************************************************************
     * Tests for listing notifications
     *************************************************************************************/

    @Test
    fun `Should throw an exception when it receives an invalid pagination request`() {
        assertThrows<IllegalArgumentException> { notificationService.latestNotifications(PageRequest.of(0, -1)) }
    }

    @Test
    fun `Should return a few recent notifications as expected`() {
        val sms = SmsNotification(recipients = setOf("0912xxx"), message = "Hi")
        Thread.sleep(1000)
        val email = EmailNotification(recipients = setOf("a@g.com"), subject = "Hi", body = "Hi")
        Thread.sleep(1000)
        val push = PushNotification(topic = "/topics/updates", title = "Hi", body = "Hi")

        notificationRepository.saveAll(setOf(sms, email, push))

        val firstTwo = PageRequest.of(0, 2)
        val notifications = notificationService.latestNotifications(firstTwo)

        assertThat(notifications.number).isEqualTo(0)
        assertThat(notifications.size).isEqualTo(2)
        assertThat(notifications.numberOfElements).isEqualTo(2)
        assertThat(notifications.content[0].id).isEqualTo(push.id)
        assertThat(notifications.content[1].id).isEqualTo(email.id)
    }

    /**
     * Test configuration to pick up messaging and notification related configurations.
     */
    @TestConfiguration
    @Import(MessagingConfig::class, NotificationConfig::class)
    @ComponentScan(basePackageClasses = [NotificationService::class])
    protected class NotificationServiceTestConfig
}

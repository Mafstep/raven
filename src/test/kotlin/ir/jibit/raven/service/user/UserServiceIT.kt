package ir.jibit.raven.service.user

import ir.jibit.raven.conf.security.AuthenticatedRequest
import ir.jibit.raven.model.user.User
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.web.client.RestTemplate

/**
 * Unit and integration tests tests for [UserService] implementation.
 *
 * @author Mehran Behnam
 */
@ActiveProfiles("test")
@ExtendWith(SpringExtension::class)
@ContextConfiguration(initializers = [ConfigFileApplicationContextInitializer::class])
class UserServiceIT {

    /**
     * Subject under test.
     */
    @Autowired
    private lateinit var userService: UserService

    /**
     * Mocked HTTP client.
     */
    @MockBean
    private lateinit var restTemplate: RestTemplate

    @AfterEach
    fun `Clear the security context`() {
        SecurityContextHolder.clearContext()
    }

    @Test
    fun `Configuration properties should work as expected`() {
        val iranPayUserService = userService as IranPayUserService

        assertThat(iranPayUserService.url).isEqualTo("http://localhost/profile")
    }

    /*************************************************************************************
     * Tests for currentAuthenticatedUser
     *************************************************************************************/

    @Test
    fun `When the client is not authenticated, it should return null`() {
        assertThat(userService.currentAuthenticatedUser())
            .isNull()
    }

    @Test
    fun `When the client is authenticated, it should return the user details`() {
        SecurityContextHolder.getContext().authentication = AuthenticatedRequest("0123")

        assertThat(userService.currentAuthenticatedUser()).isEqualTo(User("0123"))
    }

    @TestConfiguration
    @EnableConfigurationProperties(IranPayUserService::class)
    class UserServiceTestConfig
}

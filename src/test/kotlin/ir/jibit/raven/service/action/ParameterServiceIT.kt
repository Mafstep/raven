package ir.jibit.raven.service.action

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import ir.jibit.raven.MySqlServerExtension
import ir.jibit.raven.model.action.Parameter
import ir.jibit.raven.model.action.ParameterType.NUMBER
import ir.jibit.raven.model.action.ParameterType.PHONE_NUMBER
import ir.jibit.raven.repository.ParameterRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ActiveProfiles

/**
 * Integration tests for [ParameterService] service.
 *
 * @author Mehran Behnam
 */
@DataJpaTest
@DirtiesContext
@ActiveProfiles("test")
@ExtendWith(MySqlServerExtension::class)
@AutoConfigureTestDatabase(replace = NONE)
class ParameterServiceIT {

    /**
     * Subject under test.
     */
    @Autowired private lateinit var parameterService: ParameterService

    /**
     * Will be used to manage parameter entities.
     */
    @Autowired private lateinit var parameterRepository: ParameterRepository

    @BeforeEach
    fun `Flushing the DB before each test`() {
        parameterRepository.deleteAll()
    }

    /*************************************************************************************
     * Tests for creating a parameter
     *************************************************************************************/

    @Test
    fun `Create parameter should throw exception when the parameter does exist already`() {
        val amount = Parameter("amount", NUMBER)

        parameterService.createParameter(amount)

        val exception = assertThrows<ParameterAlreadyExistsException> { parameterService.createParameter(amount) }
        assertThat(exception.parameterName).isEqualTo("amount")
    }

    @Test
    fun `Create parameter should successfully persist a brand new parameter`() {
        val amount = Parameter("amount", NUMBER)

        parameterService.createParameter(amount)

        val retrieved = parameterService.getParameter("amount")
        assertThat(retrieved).isNotNull
        assertThat(retrieved!!.name).isEqualTo("amount")
        assertThat(retrieved.type).isEqualTo(NUMBER)
    }

    /*************************************************************************************
     * Tests for retrieving a parameter
     *************************************************************************************/

    @Test
    fun `Get parameter should return null for a non-existing parameter`() {
        val parameter = parameterService.getParameter("invalid")

        assertThat(parameter).isNull()
    }

    @Test
    fun `Get parameter should return the requested param when it does exist`() {
        val amount = Parameter("amount", NUMBER)
        parameterRepository.save(amount)

        val retrieved = parameterService.getParameter("amount")

        assertThat(retrieved).isNotNull
        assertThat(retrieved!!.name).isEqualTo("amount")
        assertThat(retrieved.type).isEqualTo(NUMBER)
    }

    /*************************************************************************************
     * Tests for listing parameters
     *************************************************************************************/

    @Test
    fun `List parameters should return a paginated list of parameters properly`() {
        val amount = Parameter("amount", NUMBER)
        val phoneNumber = Parameter("phoneNumber", PHONE_NUMBER)

        parameterRepository.saveAll(setOf(amount, phoneNumber))

        val parameters = parameterService.listParameters(PageRequest.of(1, 1))

        assertThat(parameters.number).isEqualTo(1)
        assertThat(parameters.size).isEqualTo(1)
        assertThat(parameters.hasPrevious()).isTrue()
        assertThat(parameters.hasNext()).isFalse()
        assertThat(parameters.content.size).isEqualTo(1)
    }

    @ParameterizedTest
    @CsvSource("0, 0", "0, -3", "-1, 2", "-1, -1")
    fun `Passing an invalid pagination information should throw an exception`(pageNumber: Int, size: Int) {
        val pageable = mock<Pageable>()
        whenever(pageable.pageNumber).thenReturn(pageNumber)
        whenever(pageable.pageSize).thenReturn(size)

        assertThrows<IllegalArgumentException> { parameterService.listParameters(pageable) }
    }

    /*************************************************************************************
     * Auxiliary stuff
     *************************************************************************************/

    /**
     * A simple test configuration just to pick up a [ParameterService] implementation.
     */
    @TestConfiguration
    protected class ActionServiceTestConfig {

        @Bean
        fun parameterService(parameterRepository: ParameterRepository) = SimpleParameterService(parameterRepository)
    }
}

package ir.jibit.raven.service.action

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import ir.jibit.raven.MySqlServerExtension
import ir.jibit.raven.model.action.Action
import ir.jibit.raven.model.action.ActionRule
import ir.jibit.raven.model.action.Parameter
import ir.jibit.raven.model.action.ParameterType
import ir.jibit.raven.model.notification.NotificationType.PUSH
import ir.jibit.raven.model.notification.NotificationType.SMS
import ir.jibit.raven.repository.ActionRepository
import ir.jibit.raven.repository.ParameterRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ActiveProfiles

/**
 * Integration tests for [ActionService] implementation.
 *
 * @author Mehran Behnam
 */
@DataJpaTest
@DirtiesContext
@ActiveProfiles("test")
@ExtendWith(MySqlServerExtension::class)
@AutoConfigureTestDatabase(replace = NONE)
class ActionServiceIT {

    /**
     * Subject under test.
     */
    @Autowired private lateinit var actionService: ActionService

    /**
     * To persist and delete action entities.
     */
    @Autowired private lateinit var actionRepository: ActionRepository

    /**
     * To persist and delete parameter entities.
     */
    @Autowired private lateinit var parameterRepository: ParameterRepository

    @BeforeEach
    fun `Flush the DB before each test`() {
        actionRepository.deleteAll()
        parameterRepository.deleteAll()
    }

    /*************************************************************************************
     * Tests for getAction
     *************************************************************************************/

    @Test
    fun `getAction should return null when the requested action does not exist`() {
        val action = actionService.getAction("invalid")

        assertThat(action).isNull()
    }

    @Test
    fun `getAction should return the requested action when we have access to such an action`() {
        val action = Action("TopUp", emptySet(), setOf(ActionRule(PUSH)))
        actionRepository.save(action)

        val topUp = actionService.getAction("TopUp")
        assertThat(topUp).isNotNull
        assertThat(topUp!!.name).isEqualTo("TopUp")
        assertThat(topUp.parameters).isEmpty()
        assertThat(topUp.actionRules).containsOnly(ActionRule(PUSH))
    }

    /*************************************************************************************
     * Tests for listActions
     *************************************************************************************/

    @ParameterizedTest
    @CsvSource("0, 0", "0, -3", "-1, 2", "-1, -1")
    fun `Passing invalid pagination information should result in IllegalArgumentException`(number: Int, size: Int) {
        val pageable = mock<Pageable>()
        whenever(pageable.pageNumber).thenReturn(number)
        whenever(pageable.pageSize).thenReturn(size)

        val exception = assertThrows<IllegalArgumentException> { actionService.listActions(pageable) }

        assertThat(exception.message).isEqualTo("Pagination information is invalid: $pageable")
    }

    @Test
    fun `listActions should paginate as expected`() {
        val giveMeOne = PageRequest.of(0, 1)
        val topUp = Action("TopUp", emptySet(), setOf(ActionRule(PUSH)))
        val purchase = Action("Purchase", emptySet(), setOf(ActionRule(PUSH)))

        actionRepository.saveAll(setOf(topUp, purchase))

        val page = actionService.listActions(giveMeOne)
        assertThat(page.size).isEqualTo(1)
        assertThat(page.hasNext()).isTrue()
        assertThat(page.hasPrevious()).isFalse()
    }

    /*************************************************************************************
     * Tests for createAction
     *************************************************************************************/

    @Test
    fun `createAction should fail when the given action has no rules`() {
        val action = Action("TopUp", emptySet(), emptySet())

        assertThrows<java.lang.IllegalArgumentException> { actionService.createAction(action) }
    }

    @Test
    fun `createAction should throw an exception the action already exists`() {
        val action = Action("TopUp", emptySet(), setOf(ActionRule(PUSH)))

        actionService.createAction(action)

        val exception = assertThrows<ActionAlreadyExistsException> { actionService.createAction(action) }
        assertThat(exception.actionName).isEqualTo("TopUp")
    }

    @Test
    fun `createAction should throw an exception when fallback references are invalid`() {
        val action = Action("TopUp", emptySet(), setOf(ActionRule(PUSH, SMS)))

        val exception = assertThrows<InvalidActionRuleFallbackReferenceException> { actionService.createAction(action) }
        assertThat(exception.invalidFallbacks).containsOnly(SMS)
    }

    @Test
    fun `createAction should throw an exception when the given parameter is invalid`() {
        val action = Action("TopUp", setOf(Parameter("amount", ParameterType.NUMBER)), setOf(ActionRule(PUSH)))

        val exception = assertThrows<ParameterNotFoundException> { actionService.createAction(action) }
        assertThat(exception.parameterName).isEqualTo("amount")
    }

    @Test
    fun `createAction should create and persist the given action when everything is OK`() {
        val parameter = Parameter("amount", ParameterType.NUMBER)
        val action = Action("TopUp", setOf(parameter), setOf(ActionRule(PUSH)))

        parameterRepository.save(parameter)
        actionService.createAction(action)

        val topUp = actionService.getAction("TopUp")
        assertThat(topUp).isNotNull
        assertThat(topUp!!.name).isEqualTo("TopUp")
    }

    /*************************************************************************************
     * Auxiliary stuff
     *************************************************************************************/

    /**
     * A simple test configuration just to pick up a [ActionService] implementation.
     */
    @TestConfiguration
    protected class ActionServiceTestConfig {

        @Bean
        fun actionService(actionRepository: ActionRepository, parameterRepository: ParameterRepository) =
                SimpleActionService(actionRepository, parameterRepository)
    }
}

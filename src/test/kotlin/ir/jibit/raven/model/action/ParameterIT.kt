package ir.jibit.raven.model.action

import ir.jibit.raven.MySqlServerExtension
import ir.jibit.raven.repository.ParameterRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ActiveProfiles

/**
 * Integration tests to test the entity mappings for [Parameter] entity.
 *
 * @author Mehran Behnam
 */
@DataJpaTest
@DirtiesContext
@ActiveProfiles("test")
@ExtendWith(MySqlServerExtension::class)
@AutoConfigureTestDatabase(replace = NONE)
class ParameterIT {

    /**
     * Will help us to store and retrieve parameters.
     */
    @Autowired private lateinit var parameterRepository: ParameterRepository

    @Test
    fun `We should be able to store and retrieve parameters as expected`() {
        val parameter = Parameter("some-param", ParameterType.NUMBER)

        parameterRepository.save(parameter)

        val retrieved = parameterRepository.findById("some-param")
        assertThat(retrieved).isNotEmpty
        assertThat(retrieved.get().name).isEqualTo("some-param")
        assertThat(retrieved.get().type).isEqualTo(ParameterType.NUMBER)
    }
}

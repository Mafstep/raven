package ir.jibit.raven.model.action

import ir.jibit.raven.MySqlServerExtension
import ir.jibit.raven.has
import ir.jibit.raven.model.notification.NotificationType
import ir.jibit.raven.model.notification.PushNotification
import ir.jibit.raven.model.notification.SmsNotification
import ir.jibit.raven.repository.ActionEventRepository
import ir.jibit.raven.repository.ActionRepository
import ir.jibit.raven.repository.NotificationRepository
import ir.jibit.raven.repository.ParameterRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ActiveProfiles

/**
 * Integration tests to test the entity mappings for [ActionEvent] entity.
 *
 * @author Mehran Behnam
 */
@DataJpaTest
@DirtiesContext
@ActiveProfiles("test")
@ExtendWith(MySqlServerExtension::class)
@AutoConfigureTestDatabase(replace = NONE)
class ActionEventIT {

    /**
     * Will be used to persist and retrieve [Action] values.
     */
    @Autowired private lateinit var actionRepository: ActionRepository

    /**
     * Will be used to persist and retrieve [Parameter] values.
     */
    @Autowired private lateinit var parameterRepository: ParameterRepository

    /**
     * Will be used to persist and retrieve [ActionEvent] values.
     */
    @Autowired private lateinit var actionEventRepository: ActionEventRepository

    /**
     * Will be used to persist and retrieve [ir.jibit.raven.model.notification.Notification] values.
     */
    @Autowired private lateinit var notificationRepository: NotificationRepository

    @Test
    fun `Should be able to persist and retrieve action events properly (Cascade notifications)`() {
        val age = Parameter("age", ParameterType.NUMBER)
        val name = Parameter("name", ParameterType.STRING)
        parameterRepository.saveAll(setOf(age, name))

        val action = Action("withdraw", setOf(age, name), setOf(ActionRule(NotificationType.PUSH)))
        actionRepository.save(action)

        val sms= SmsNotification(recipients = setOf("0912 xxx"), message = "Hello")
        val push = PushNotification(topic = "/topics/updates", title = "Title", body = "Body")
        notificationRepository.saveAll(setOf(sms, push))

        val params = mapOf("phone_number" to "0912 xxx", "token" to "123", "amount" to "100")
        val event = ActionEvent(action = action, actionParameters = params, notifications = setOf(sms, push), user = "me")

        actionEventRepository.save(event)

        val retrieved = actionEventRepository.findById(event.id)
        assertThat(retrieved).isNotEmpty
        assertThat(retrieved.get().id).isEqualTo(event.id)
        assertThat(retrieved.get().action).isEqualTo(action)
        assertThat(retrieved.get().whenHappened).isEqualTo(event.whenHappened)
        assertThat(retrieved.get().user).isEqualTo(event.user)
        assertThat(retrieved.get().actionParameters).isEqualTo(event.actionParameters)
        assertThat(retrieved.get().notifications).hasSize(2)
        assertThat(retrieved.get().notifications).has("One sms and one push") {
            all {
                when (it) {
                    is SmsNotification -> it.message == "Hello" && it.recipients.contains("0912 xxx")
                    is PushNotification -> it.topic == "/topics/updates"
                    else -> true
                }
            }
        }
    }
}

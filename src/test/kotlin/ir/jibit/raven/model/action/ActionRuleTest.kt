package ir.jibit.raven.model.action

import ir.jibit.raven.model.notification.NotificationType.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

/**
 * Unit tests for [ActionRule] entity.
 *
 * @author Mehran Behnam
 */
class ActionRuleTest {

    @Test
    fun `Two action rules are equal iff they have the same notification type`() {
        val push = ActionRule(PUSH)
        val sms = ActionRule(SMS)
        val againPush = ActionRule(PUSH, fallbackFor = SMS)

        // Reference equality should still work
        assertThat(push == push).isTrue()

        // How about hash based collections?
        assertThat(hashSetOf(push, againPush, sms)).hasSize(2)
        assertThat(hashSetOf(push, againPush, sms)).contains(ActionRule(PUSH, SMS))

        // How about identity based equality?
        assertThat(push == againPush).isTrue()

        // How about a wrong type to compare?
        assertThat(push == Any()).isFalse()
    }
}

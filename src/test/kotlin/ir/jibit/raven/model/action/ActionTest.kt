package ir.jibit.raven.model.action

import ir.jibit.raven.model.notification.NotificationType
import ir.jibit.raven.model.notification.NotificationType.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments.arguments
import org.junit.jupiter.params.provider.MethodSource

/**
 * Unit tests for [Action] entity.
 *
 * @author Mehran Behnam
 */
class ActionTest {

    @Test
    fun `Two actions are equal iff they have the same name`() {
        val first = Action("first", emptySet(), emptySet())
        val sec = Action("second", setOf(Parameter("name", ParameterType.STRING)), emptySet())
        val firstAgain = Action("first", setOf(Parameter("name", ParameterType.STRING)), emptySet())

        // How about reference equality?
        assertThat(first == first).isTrue()

        // How about hash based collections?
        assertThat(hashSetOf(first, sec, firstAgain)).hasSize(2)
        assertThat(hashSetOf(first, sec, firstAgain)).contains(Action("first", emptySet(), setOf(ActionRule(PUSH))))

        // How about identity based equality?
        assertThat(first == firstAgain).isTrue()

        // How about a wrong type to compare?
        assertThat(first == Any()).isFalse()
    }

    @ParameterizedTest
    @MethodSource("paramsForFindInvalidFallbackReferences")
    fun `findInvalidFallbackReferences should return all invalid fallback references`(action: Action,
                                                                                      expectedReferences: Set<NotificationType>) {
        assertThat(action.findInvalidFallbackReferences())
                .containsOnly(*expectedReferences.toTypedArray())
    }

    companion object {
        @JvmStatic
        fun paramsForFindInvalidFallbackReferences() = listOf(
                arguments(withRules(ActionRule(PUSH, PUSH)), setOf(PUSH)),
                arguments(withRules(ActionRule(PUSH, SMS), ActionRule(EMAIL)), setOf(SMS)),
                arguments(withRules(ActionRule(PUSH), ActionRule(EMAIL, PUSH)), setOf<NotificationType>()),
                arguments(withRules(ActionRule(PUSH), ActionRule(EMAIL)), setOf<NotificationType>())
        )

        private fun withRules(vararg rules: ActionRule) = Action("test", emptySet(), rules.toSet())
    }
}

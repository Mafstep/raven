package ir.jibit.raven.model.action

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

/**
 * Unit tests for [Parameter] entity.
 *
 * @author Mehran Behnam
 */
class ParameterTest {

    @Test
    fun `equals method should work as expected`() {
        val p1 = Parameter("first", ParameterType.NUMBER)
        val p2 = Parameter("second", ParameterType.PHONE_NUMBER)
        val p3 = Parameter("first", ParameterType.NUMBER)

        // De-duplication should work with set
        val setOfParams = setOf(p1, p2, p3)
        assertThat(setOfParams).hasSize(2)

        // Reference equality should work
        assertThat(p1 == p1).isTrue()

        // Should be equal based on name
        assertThat(p1).isEqualTo(p3)

        // hashCode is compatible with equals
        assertThat(Parameter("second", ParameterType.PHONE_NUMBER) in setOfParams).isTrue()

        assertThat(p1 == Any()).isFalse()
    }
}

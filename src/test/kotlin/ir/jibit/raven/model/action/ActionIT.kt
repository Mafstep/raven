package ir.jibit.raven.model.action

import ir.jibit.raven.MySqlServerExtension
import ir.jibit.raven.model.notification.NotificationType.PUSH
import ir.jibit.raven.model.notification.NotificationType.SMS
import ir.jibit.raven.repository.ActionRepository
import ir.jibit.raven.repository.ParameterRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ActiveProfiles

/**
 * Integration tests to test the entity mappings for [Action] entity.
 *
 * @author Mehran Behnam
 */
@DataJpaTest
@DirtiesContext
@ActiveProfiles("test")
@ExtendWith(MySqlServerExtension::class)
@AutoConfigureTestDatabase(replace = NONE)
class ActionIT {

    /**
     * Will be used to persist and retrieve [Action] values.
     */
    @Autowired private lateinit var actionRepository: ActionRepository

    /**
     * Will be used to persist and retrieve [Parameter] values.
     */
    @Autowired private lateinit var parameterRepository: ParameterRepository

    @Test
    fun `Should be able persist and retrieve actions appropriately`() {
        val age = Parameter("age", ParameterType.NUMBER)
        val birthDay = Parameter("birthday", ParameterType.DATE_TIME)
        parameterRepository.saveAll(listOf(age, birthDay))

        val actionRules = setOf(ActionRule(PUSH), ActionRule(SMS, fallbackFor = PUSH))
        val action = Action("birthday", setOf(age, birthDay), actionRules)

        actionRepository.save(action)

        val retrieved = actionRepository.findById("birthday")
        assertThat(retrieved).isNotEmpty
        assertThat(retrieved.get().name).isEqualTo("birthday")
        assertThat(retrieved.get().parameters).containsAll(listOf(age, birthDay))
        assertThat(retrieved.get().actionRules).containsAll(actionRules)
    }
}

package ir.jibit.raven.model.token

import ir.jibit.raven.model.token.TokenType.*
import ir.jibit.raven.web.notification.dto.NewNotificationDto
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments.arguments
import org.junit.jupiter.params.provider.MethodSource

/**
 * Unit tests for [RegistrationToken] domain.
 *
 * @author Mehran Behnam
 */
class RegistrationTokenTest {

    @ParameterizedTest
    @MethodSource("provideTokens")
    fun `Two tokens are identical iff both have the same token value`(t1: Any,
                                                                      t2: Any,
                                                                      expected: Boolean) {
        assertThat(t1 == t2).isEqualTo(expected)
    }

    companion object {
        private val token = RegistrationToken(user = "123", tokenType = ANDROID, appVersion = "1.2.3", token = "token")

        @JvmStatic
        fun provideTokens() = listOf(
            arguments(token, token, true),
            arguments(
                RegistrationToken(user = "123", tokenType = ANDROID, appVersion = "1.2.3", token = "token"),
                RegistrationToken(user = "123", tokenType = ANDROID, appVersion = "1.2.3", token = "other token"),
                false
            ),
            arguments(
                RegistrationToken(user = "123", tokenType = ANDROID, appVersion = "1.2.3", token = "token"),
                RegistrationToken(user = "123", tokenType = IOS, appVersion = "1.2.3", token = "token"),
                true
            ),
            arguments(
                RegistrationToken(user = "123", tokenType = ANDROID, appVersion = "1.2.3", token = "token"),
                RegistrationToken(user = "123", tokenType = ANDROID, appVersion = "1.2.3", token = "token"),
                true
            ),
            arguments(
                NewNotificationDto(),
                RegistrationToken(user = "123", tokenType = ANDROID, appVersion = "1.2.3", token = "token"),
                false
            )
        )
    }
}

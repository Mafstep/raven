package ir.jibit.raven.model.token

import ir.jibit.raven.MySqlServerExtension
import ir.jibit.raven.repository.RegistrationTokenRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ActiveProfiles

/**
 * Integration tests to test the entity mappings for [RegistrationToken] entity.
 *
 * @author Mehran Behnam
 */
@DataJpaTest
@DirtiesContext
@ActiveProfiles("test")
@ExtendWith(MySqlServerExtension::class)
@AutoConfigureTestDatabase(replace = NONE)
class RegistrationTokenIT {

    /**
     * Will be used to persist and retrieve a [RegistrationToken].
     */
    @Autowired private lateinit var repository: RegistrationTokenRepository

    @Test
    fun `Should be able to persist and retrieve the registration token properly`() {
        val token = RegistrationToken(user = "me", token = "123", appVersion = "1.0.0", tokenType = TokenType.IOS,
            metadata = mapOf("os_version" to "12.0.0"))

        repository.save(token)

        val retrieved = repository.findById(token.id)
        assertThat(retrieved.get().id).isEqualTo(token.id)
        assertThat(retrieved.get().token).isEqualTo(token.token)
        assertThat(retrieved.get().user).isEqualTo(token.user)
        assertThat(retrieved.get().registrationDate).isEqualTo(token.registrationDate)
        assertThat(retrieved.get().metadata).isEqualTo(token.metadata)
    }
}

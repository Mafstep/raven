package ir.jibit.raven.model.converter

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments.arguments
import org.junit.jupiter.params.provider.MethodSource

/**
 * Unit tests for [JsonConverter].
 *
 * @author Mehran Behnam
 */
class JsonConverterTest {

    /**
     * Subject under test.
     */
    private val converter = JsonConverter()

    @ParameterizedTest
    @MethodSource("paramsForConvertToDatabaseColumn")
    fun `convertToDatabaseColumn should convert given maps to JSON strings`(map: Map<String, String>?, expected: String) {
        assertThat(converter.convertToDatabaseColumn(map))
                .isEqualTo(expected)
    }

    @ParameterizedTest
    @MethodSource("paramsForConvertToEntityAttribute")
    fun `convertToEntityAttribute should convert the JSON strings back to maps`(json: String?, expected: Map<String, String>) {
        assertThat(converter.convertToEntityAttribute(json))
                .isEqualTo(expected)
    }

    companion object {
        @JvmStatic fun paramsForConvertToDatabaseColumn() = listOf(
                arguments(null, "{}"),
                arguments(emptyMap<String, String>(), "{}"),
                arguments(mapOf("name" to "me"), """{"name":"me"}"""),
                arguments(mapOf("name" to "me", "expertise" to "SE"), """{"name":"me","expertise":"SE"}""")
        )

        @JvmStatic fun paramsForConvertToEntityAttribute() = listOf(
                arguments(null, emptyMap<String, String>()),
                arguments("{}", emptyMap<String, String>()),
                arguments("""{"name":"me"}""", mapOf("name" to "me")),
                arguments("""{"name":"me","expertise":"SE"}""", mapOf("name" to "me", "expertise" to "SE"))
        )
    }
}

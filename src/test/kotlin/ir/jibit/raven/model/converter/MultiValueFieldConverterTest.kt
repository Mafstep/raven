package ir.jibit.raven.model.converter

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments.arguments
import org.junit.jupiter.params.provider.MethodSource

/**
 * Unit tests for [MultiValueFieldConverter] attribute converter.
 *
 * @author Mehran Behnam
 */
class MultiValueFieldConverterTest {

    /**
     * Subject under test.
     */
    private val converter = MultiValueFieldConverter()

    @ParameterizedTest
    @MethodSource("paramsForConvertToDatabaseColumn")
    fun `convertToDatabaseColumn should convert the given set to a CSV properly`(values: Set<String>?, expected: String) {
        assertThat(converter.convertToDatabaseColumn(values))
                .isEqualTo(expected)
    }

    @ParameterizedTest
    @MethodSource("paramsForConvertToEntityAttribute")
    fun `convertToEntityAttribute should convert CSV back to the original set`(csv: String?, expected: Set<String>) {
        assertThat(converter.convertToEntityAttribute(csv))
                .isEqualTo(expected)
    }

    companion object {
        @JvmStatic fun paramsForConvertToDatabaseColumn() = listOf(
                arguments(null, ""),
                arguments(emptySet<String>(), ""),
                arguments(setOf("single"), "single"),
                arguments(setOf("me", "you", "her"), "me,you,her")
        )

        @JvmStatic fun paramsForConvertToEntityAttribute() = listOf(
                arguments(null, emptySet<String>()),
                arguments("", emptySet<String>()),
                arguments("   ", emptySet<String>()),
                arguments("single", setOf("single")),
                arguments("me,you,her", setOf("me", "you", "her"))
        )
    }
}

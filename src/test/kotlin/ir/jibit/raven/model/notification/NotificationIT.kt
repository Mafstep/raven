package ir.jibit.raven.model.notification

import ir.jibit.raven.MySqlServerExtension
import ir.jibit.raven.repository.NotificationRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ActiveProfiles

/**
 * Integration tests to test the entity mapping for the [Notification] entity.
 * and its subclasses.
 *
 * @author Mehran Behnam
 */
@DataJpaTest
@DirtiesContext
@ActiveProfiles("test")
@ExtendWith(MySqlServerExtension::class)
@AutoConfigureTestDatabase(replace = NONE)
class NotificationIT {

    /**
     * Helps us to store and retrieve a few notifications from DB.
     */
    @Autowired private lateinit var notificationRepository: NotificationRepository

    @BeforeEach
    fun `Clear the data before each test`() {
        notificationRepository.deleteAll()
    }

    @Test
    fun `SMS Notification should be persisted as expected`() {
        val toPersist = SmsNotification(recipients = setOf("0912 xxx", "0912 yyy"), message = "Hello")
        toPersist.ttl = 10
        toPersist.data = mapOf("name" to "ali", "age" to "29")

        notificationRepository.save(toPersist)

        val notification = notificationRepository.findById(toPersist.id)
        assertThat(notification).isNotEmpty

        assertThat(notification.get().notificationState).isEqualTo(NotificationState.SUBMITTED)
        assertThat(notification.get().processLog).isNull()
        assertThat(notification.get().ttl).isEqualTo(10)
        assertThat(notification.get().data).isEqualTo(mapOf("name" to "ali", "age" to "29"))
        assertThat(notification.get().whenSubmitted).isEqualTo(toPersist.whenSubmitted)
        assertThat(notification.get()).isInstanceOf(SmsNotification::class.java)

        val sms = notification.get() as SmsNotification
        assertThat(sms.recipients).isEqualTo(setOf("0912 xxx", "0912 yyy"))
        assertThat(sms.message).isEqualTo("Hello")
    }

    @Test
    fun `Email notification should get persisted as expected`() {
        val toPersist = EmailNotification(setOf("a@g.com", "b@g.com"), subject = "Hello", body = "Dear Robot, ...")
        toPersist.notificationState = NotificationState.FAILED
        toPersist.processLog = "Failed..."

        notificationRepository.save(toPersist)

        val notification = notificationRepository.findById(toPersist.id)
        assertThat(notification).isNotEmpty

        assertThat(notification.get().id).isEqualTo(toPersist.id)
        assertThat(notification.get().data).isEmpty()
        assertThat(notification.get().ttl).isEqualTo(-1)
        assertThat(notification.get().processLog).isEqualTo("Failed...")
        assertThat(notification.get().notificationState).isEqualTo(NotificationState.FAILED)
        assertThat(notification.get()).isInstanceOf(EmailNotification::class.java)
        assertThat(notification.get().whenSubmitted).isEqualTo(toPersist.whenSubmitted)

        val email = notification.get() as EmailNotification
        assertThat(email.recipients).isEqualTo(setOf("a@g.com", "b@g.com"))
        assertThat(email.subject).isEqualTo("Hello")
        assertThat(email.body).isEqualTo("Dear Robot, ...")
    }

    @Test
    fun `Push notification should get persisted as expected`() {
        val toPersist = PushNotification(topic = "/topic/updates", recipientTokens = setOf("123", "456"),
                title = "Title", body = "Body")

        notificationRepository.save(toPersist)

        val notification = notificationRepository.findById(toPersist.id)
        assertThat(notification).isNotEmpty
        assertThat(notification.get().id).isEqualTo(toPersist.id)
        assertThat(notification.get().notificationState).isEqualTo(NotificationState.SUBMITTED)
        assertThat(notification.get().ttl).isEqualTo(-1)
        assertThat(notification.get().data).isEmpty()
        assertThat(notification.get().processLog).isNull()
        assertThat(notification.get().whenSubmitted).isEqualTo(toPersist.whenSubmitted)
        assertThat(notification.get()).isInstanceOf(PushNotification::class.java)

        val push = notification.get() as PushNotification
        assertThat(push.recipientTokens).containsOnly("123", "456")
        assertThat(push.topic).isEqualTo("/topic/updates")
        assertThat(push.title).isEqualTo("Title")
        assertThat(push.body).isEqualTo("Body")
    }
}

package ir.jibit.raven

import com.rabbitmq.client.ConnectionFactory
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.autoconfigure.ImportAutoConfiguration
import org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.lang.annotation.Inherited

/**
 * A test [ImportAutoConfiguration] for typical RabbitMQ use cases which auto-configures
 * the [RabbitAutoConfiguration] and starts a test RabbitMQ server before all tests.
 *
 * @author Mehran Behnam
 */
@Inherited
@MustBeDocumented
@Target(AnnotationTarget.CLASS)
@ImportAutoConfiguration(RabbitAutoConfiguration::class)
@ExtendWith(SpringExtension::class, RabbitMqServerExtension::class)
annotation class AutoConfigureRabbit

/**
 * A JUnit extension to start a RabbitMQ server before all tests and tear it down after
 * running those tests. This extension will be blocked until the RabbitMQ broker is ready
 * to accept requests.
 *
 * [JUnit 5: Extension Model](https://junit.org/junit5/docs/current/user-guide/#extensions)
 *
 * @author
 */
class RabbitMqServerExtension : DockerContainer(
        "rabbitmq",
        "3.7-alpine",
        emptyList(),
        { waitForRabbitToStart() },
        PortMapping(5672, 5672)
)

private fun waitForRabbitToStart() {
    val factory = ConnectionFactory()
    factory.host = System.getenv("SPRING_RABBITMQ_HOST") ?: "localhost"
    while (true) {
        try {
            factory.newConnection().use {}
            break
        } catch (ignored: Throwable) {}
    }
}

package ir.jibit.raven.util

/**
 * Returns `true` if the receiving string collection is null, empty or filled with blank items.
 *
 * @author Mehran Behnam
 */
fun Collection<String>?.isCompletelyEmpty(): Boolean = this == null || isEmpty() || all { it.isBlank() }

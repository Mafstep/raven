package ir.jibit.raven.util

import org.springframework.data.domain.Pageable

/**
 * Extension function on [Pageable] to test its validity.
 *  - The page number should be greater than or equal to zero.
 *  - The page size should be greater than zero.
 *
 * @author Mehran Behnam
 */
fun Pageable.isValid(): Boolean = pageSize > 0 && pageNumber >= 0

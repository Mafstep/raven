package ir.jibit.raven.repository

import ir.jibit.raven.model.token.RegistrationToken
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

/**
 * Provides data access operations for [RegistrationToken] entity.
 *
 * @author Mehran Behnam
 */
interface RegistrationTokenRepository : JpaRepository<RegistrationToken, UUID> {

    /**
     * Returns all device tokens registered for the given [userIdentifiers].
     */
    fun findAllByUserIn(userIdentifiers: Set<String>): Set<RegistrationToken>
}

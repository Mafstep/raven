package ir.jibit.raven.repository

import ir.jibit.raven.model.notification.Notification
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Slice
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import java.util.*

/**
 * Provides data access operations over [Notification] entity.
 *
 * @author Mehran Behnam
 */
interface NotificationRepository : JpaRepository<Notification, UUID> {

    /**
     * Retrieves a paginated list of recent submitted notifications:
     *  - [pageable] Encapsulates the pagination request details.
     */
    @Query("select n from Notification n order by n.whenSubmitted desc")
    fun latestNotifications(pageable: Pageable): Slice<Notification>
}

package ir.jibit.raven.repository

import ir.jibit.raven.model.action.Action
import org.springframework.data.jpa.repository.JpaRepository

/**
 * Provides data access operations over [Action] entity.
 *
 * @author Mehran Behnam
 */
interface ActionRepository : JpaRepository<Action, String>

package ir.jibit.raven.repository

import ir.jibit.raven.model.action.Parameter
import org.springframework.data.jpa.repository.JpaRepository

/**
 * Provides data access operations over [Parameter] entity.
 *
 * @author Mehran Behnam
 */
interface ParameterRepository : JpaRepository<Parameter, String>

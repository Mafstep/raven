package ir.jibit.raven.repository

import ir.jibit.raven.model.action.ActionEvent
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

/**
 * Provides data access operations over [ActionEvent] entity.
 *
 * @author Mehran Behnam
 */
interface ActionEventRepository : JpaRepository<ActionEvent, UUID>

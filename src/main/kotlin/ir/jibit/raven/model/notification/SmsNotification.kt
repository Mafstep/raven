package ir.jibit.raven.model.notification

import ir.jibit.raven.model.PhoneNumber
import ir.jibit.raven.model.converter.MultiValueFieldConverter
import javax.persistence.Column
import javax.persistence.Convert
import javax.persistence.DiscriminatorValue
import javax.persistence.Entity

/**
 * Encapsulates notification parameters specific for SMS notification type.
 *
 * @author Mehran Behnam
 */
@Entity
@DiscriminatorValue("SMS")
class SmsNotification(

    /**
     * Who will receive the SMS notification?
     */
    @Column(columnDefinition = "text")
    @Convert(converter = MultiValueFieldConverter::class)
    val recipients: Set<PhoneNumber>,

    /**
     * Represents the SMS text message.
     */
    @Column(columnDefinition = "text")
    val message: String

) : Notification()

package ir.jibit.raven.model.notification

import ir.jibit.raven.model.Email
import ir.jibit.raven.model.converter.MultiValueFieldConverter
import javax.persistence.Column
import javax.persistence.Convert
import javax.persistence.DiscriminatorValue
import javax.persistence.Entity

/**
 * Encapsulates notification parameters specific for Email notification type.
 *
 * @author Mehran Behnam
 */
@Entity
@DiscriminatorValue("EMAIL")
class EmailNotification(

    /**
     * Who will receive the notification?
     */
    @Column(columnDefinition = "text")
    @Convert(converter = MultiValueFieldConverter::class)
    val recipients: Set<Email>,

    /**
     * Encapsulates the email subject.
     */
    @Column(columnDefinition = "text")
    val subject: String,

    /**
     * Encapsulates the email body.
     */
    @Column(columnDefinition = "text")
    val body: String

) : Notification()

package ir.jibit.raven.model.notification

import ir.jibit.raven.model.converter.MultiValueFieldConverter
import javax.persistence.Column
import javax.persistence.Convert
import javax.persistence.DiscriminatorValue
import javax.persistence.Entity

/**
 * Encapsulates push specific notification details.
 *
 * @author Mehran Behnam
 */
@Entity
@DiscriminatorValue("PUSH")
class PushNotification(

    /**
     * Represents the push notification title.
     */
    @Column(columnDefinition = "text")
    val title: String,

    /**
     * Represents the push notification body.
     */
    @Column(columnDefinition = "text")
    val body: String,

    /**
     * Represents the registration tokens to send a notification to.
     */
    @Column(columnDefinition = "text")
    @Convert(converter = MultiValueFieldConverter::class)
    val recipientTokens: Set<String>? = null,

    /**
     * Represents the topic name to send a message to.
     */
    @Column(columnDefinition = "text")
    val topic: String? = null

) : Notification()

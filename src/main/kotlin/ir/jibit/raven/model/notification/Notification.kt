package ir.jibit.raven.model.notification

import ir.jibit.raven.model.converter.JsonConverter
import ir.jibit.raven.model.notification.NotificationState.*
import org.hibernate.annotations.Type
import java.time.Clock
import java.time.Instant
import java.util.*
import javax.persistence.*

/**
 * Base class for all other notification types which encapsulates common properties across all
 * notifications. De-normalization is a recurring theme in [Notification] and its subclasses.
 * The rationale behind this de-normalization is the fact that we're not gonna perform some queries
 * based on those de-normalized fields.
 *
 * // TODO In an ideal world, the Kotlin JPA plugin should've add those open keywords for us!
 *
 * @author Mehran Behnam
 */
@Entity
@Table(name = "notifications")
@DiscriminatorColumn(name = "notification_type")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
open class Notification(

    /**
     * Represents a unique identifier for the notification.
     */
    @Id
    @Type(type = "uuid-char")
    @Column(columnDefinition = "char(36)")
    val id: UUID = UUID.randomUUID(),

    /**
     * Arbitrary key/value pairs to be passed as notification parameters. Each notification type
     * can have its own medium-specific, e.g. SMS or Push, set of notification parameters
     */
    @Column(columnDefinition = "text")
    @Convert(converter = JsonConverter::class)
    open var data: Map<String, String> = emptyMap(),

    /**
     * How long (in seconds) the message should be kept in Raven storage if the notification destination
     * is offline. The -1 value means forever and should be avoided.
     */
    open var ttl: Int = -1,

    /**
     * Represents the notification state.
     */
    @Enumerated(EnumType.STRING)
    open var notificationState: NotificationState = NotificationState.SUBMITTED,

    /**
     * If we failed to send a particular notification, this will encapsulates the possible stacktrace
     * for the failure. Otherwise, this field would encapsulates the metadata about the successful
     * operation.
     */
    @Column(columnDefinition = "text")
    open var processLog: String? = null,

    /**
     * When the notification request got submitted?
     */
    open val whenSubmitted: Instant = Instant.now(Clock.systemUTC())
)

/**
 * Represents a notification state. Each notification starts in the [SUBMITTED] state. When a particular
 * component decides to process a notification, the notification should be in [PROCESSING] state. If the
 * notification got successfully sent to its intended destination, it should be in [DONE] state. Otherwise,
 * the terminal state of the notification would be [FAILED].
 */
enum class NotificationState {

    /**
     * The notification just got submitted.
     */
    SUBMITTED,

    /**
     * The notification is currently being processed.
     */
    PROCESSING,

    /**
     * The notification is sent successfully.
     */
    DONE,

    /**
     * We failed to send the notifications.
     */
    FAILED
}

/**
 * Represents all possible notification types.
 *
 * @author Mehran Behnam
 */
enum class NotificationType {

    /**
     * The SMS or TEXT message notification.
     */
    SMS,

    /**
     * The EMAIL notification type.
     */
    EMAIL,

    /**
     * Push notifications for smart phones and web browsers.
     */
    PUSH
}

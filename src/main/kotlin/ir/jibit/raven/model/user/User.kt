package ir.jibit.raven.model.user

/**
 * Represents a user profile info containing details such as [phoneNumber].
 *
 * @author Nazila Akbari
 */
data class User(

    /**
     * User phoneNumber number.
     */
    val phoneNumber: String
)

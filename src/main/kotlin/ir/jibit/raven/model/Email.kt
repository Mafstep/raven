package ir.jibit.raven.model

/**
 * Type alias for email addresses.
 *
 * @author Mehran Behnam
 */
typealias Email = String

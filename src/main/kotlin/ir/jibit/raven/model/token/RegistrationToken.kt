package ir.jibit.raven.model.token

import ir.jibit.raven.model.converter.JsonConverter
import org.hibernate.annotations.Type
import java.time.Clock
import java.time.Instant
import java.util.*
import javax.persistence.*

/**
 * Represents a device token for a specific user on a specific device. Using this
 * token we can identify the user to send a push or web notification for him/her.
 * This token might represent an iOS client, Android client or web client.
 *
 * @author Mehran Behnam
 */
@Entity
@Table(name = "registration_tokens")
class RegistrationToken(

    /**
     * A unique identifier for the to-be-registered token.
     */
    @Id
    @Type(type = "uuid-char")
    @Column(columnDefinition = "char(36)")
    val id: UUID = UUID.randomUUID(),

    /**
     * Represents the user identifier.
     */
    val user: String,

    /**
     * The device token.
     */
    @Column(columnDefinition = "text")
    val token: String,

    /**
     * The client app version.
     */
    @Column(columnDefinition = "varchar(20)")
    val appVersion: String,

    /**
     * Determines the target device for the token. Currently we only support iOS, Android and Web targets.
     */
    @Enumerated(EnumType.STRING)
    val tokenType: TokenType,

    /**
     * Might encapsulate more metadata about registered token such as OS version or device name.
     */
    @Convert(converter = JsonConverter::class)
    @Column(columnDefinition = "text")
    val metadata: Map<String, String>? = emptyMap(),

    /**
     * When was the token registered for the first time?
     */
    val registrationDate: Instant = Instant.now(Clock.systemUTC())
) {

    /**
     * Two tokens are identical iff they have the same [token] value.
     */
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is RegistrationToken) return false

        if (token != other.token) return false

        return true
    }

    /**
     * An [equals] compatible hashcode for tokens.
     */
    override fun hashCode(): Int {
        return token.hashCode()
    }
}

/**
 * Determines the target device type for the notification.
 *
 * @author Mehran Behnam
 */
enum class TokenType {

    /**
     * The token belongs to an Android device.
     */
    ANDROID,

    /**
     * The token belongs to an iOS device.
     */
    IOS,

    /**
     * The token belongs to a web client.
     */
    WEB
}

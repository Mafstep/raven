package ir.jibit.raven.model.action

import ir.jibit.raven.model.notification.NotificationType
import javax.persistence.*

/**
 * Represents a pre-defined business action. These actions are usually have a one-to-one
 * correspondence with user actions such as withdrawing, transferring, paying bills, etc.
 *
 * ## Action Parameters
 * Each action can be parameterized by a set of parameters. For example, withdraw action
 * can be parameterized over the `amount`, the `account owner`, etc.
 *
 * ## Action Rules
 * Action rules are acting like a bridge between actions and notifications. That is, they're
 * determining how to notify users about an action.
 *
 * @see [Parameter]
 * @see [ActionRule]
 * @author Mehran Behnam
 */
@Entity
@Table(name = "actions")
class Action(

    /**
     * Represents the unique action name.
     */
    @Id
    val name: String,

    /**
     * The parameters that we're gonna parameterize the action around them.
     */
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "action_parameters", joinColumns = [JoinColumn(name = "action_name")],
        inverseJoinColumns = [JoinColumn(name = "parameter_name")])
    val parameters: Set<Parameter>,

    /**
     * Determines how we're gonna notify users for this action.
     */
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "action_rules", joinColumns = [JoinColumn(name = "action_name")])
    val actionRules: Set<ActionRule>
) {

    /**
     * Actions can be uniquely identified by their names, so this equals method makes sure that
     * two different [Action] objects are considered equal iff their name are equal.
     */
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Action) return false

        return name == other.name
    }

    /**
     * An equals-compatible method implementation for hashcode.
     */
    override fun hashCode(): Int {
        return name.hashCode()
    }

    /**
     * Dependent notification types, i.e. [ActionRule.fallbackFor], should have a valid reference
     * to a previously defined [ActionRule.notificationType]. Any [ActionRule] violating this rule
     * considered as an invalid reference.
     */
    fun findInvalidFallbackReferences(): Set<NotificationType> {
        val notifications = actionRules.map { it.notificationType }

        return actionRules
            .filter { isInvalidFallback(it, notifications) }
            .map { it.fallbackFor!! }
            .toSet()
    }

    private fun isInvalidFallback(actionRule: ActionRule, notifications: List<NotificationType>): Boolean {
        val fallbackAndPrimaryTypeAreEqual = actionRule.fallbackFor == actionRule.notificationType
        val danglingFallbackReference = actionRule.fallbackFor != null && actionRule.fallbackFor !in notifications

        return fallbackAndPrimaryTypeAreEqual || danglingFallbackReference
    }
}

/**
 * Actions rules are suppose to bridge the gap between notifications and actions. Each
 * action rule consists of one notification type and one optional fallback notification.
 *
 * @author Mehran Behnam
 */
@Embeddable
class ActionRule(

    /**
     * Determines the medium we're gonna use to send a notification through that. For example,
     * when an action have a `PUSH` action rule, it means we're gonna send a push notification
     * to the user when the action happens.
     */
    @Enumerated(EnumType.STRING)
    val notificationType: NotificationType,

    /**
     * Sometimes we're gonna register a [NotificationType] conditionally. For example, if we only
     * care to send `SMS` notifications when the corresponding `PUSH` has been failed, we can
     * set the [fallbackFor] field:
     * ```
     *     val smsWhenPushIsFailed = ActionRule(notificationType = SMS, fallbackFor = PUSH)
     * ```
     */
    @Enumerated(EnumType.STRING)
    val fallbackFor: NotificationType? = null
) {

    /**
     * An [ActionRule] can be uniquely identified by its [notificationType]. So, two different [ActionRule]
     * objects are equal iff they have the same [notificationType]s.
     */
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is ActionRule) return false

        return notificationType == other.notificationType
    }

    /**
     * An equals-compatible method implementation for hashcode.
     */
    override fun hashCode(): Int {
        return notificationType.hashCode()
    }
}

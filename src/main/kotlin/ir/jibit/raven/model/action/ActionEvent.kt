package ir.jibit.raven.model.action

import ir.jibit.raven.model.converter.JsonConverter
import ir.jibit.raven.model.notification.Notification
import org.hibernate.annotations.Type
import java.time.Clock
import java.time.Instant
import java.util.*
import javax.persistence.*

/**
 * Each [Action] defines a blueprint for a business action. For example, it represents how a
 * purchase should look like. But, when a user purchases something, we can't model this event
 * with an [Action]. This is where [ActionEvent]s come in. That is, [ActionEvent]s are going to
 * represent an actual action event that happened in our system. Using an OOP analogy, [Action]s
 * are like `Classes` and [ActionEvent]s are like `Objects`.
 *
 * @author Mehran Behnam
 */
@Entity
@Table(name = "action_events")
class ActionEvent(

    /**
     * A random identifier for the action event.
     */
    @Id
    @Type(type = "uuid-char")
    @Column(columnDefinition = "char(36)")
    val id: UUID = UUID.randomUUID(),

    /**
     * The [Action] this event belongs to.
     */
    @OneToOne
    @JoinColumn(name = "action_name")
    val action: Action,

    /**
     * The parameters for the this event. For example, for an event like withdrawal, the amount and
     * account number are the parameters.
     */
    @Column(columnDefinition = "text")
    @Convert(converter = JsonConverter::class)
    val actionParameters: Map<String, String>,

    /**
     * Id of the user who triggered the action.
     */
    @Column(columnDefinition = "varchar(100)")
    val user: String,

    /**
     * Represents the notifications we should send for this particular action event.
     */
    @OneToMany
    @JoinTable(name = "action_event_notifications", joinColumns = [JoinColumn(name = "action_event_id")],
        inverseJoinColumns = [JoinColumn(name = "notification_id")])
    val notifications: Set<Notification>,

    /**
     * When the action event happened.
     */
    val whenHappened: Instant = Instant.now(Clock.systemUTC())
)

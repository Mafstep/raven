package ir.jibit.raven.model.action

import javax.persistence.*

/**
 * Encapsulates an action specific parameter. Each [Action] may contain a few parameters.
 * For example, a money withdrawal action may contain the following parameters:
 *  - The `amount` parameter representing the withdrawn amount.
 *  - The `balance` representing the account balance after withdrawal.
 *  - The `transaction date` parameter representing the withdrawal time.
 *
 * As you can see from the preceding example, each parameter can have a:
 *  - Name which describes what the parameter contains (Encapsulated in the [name] field).
 *  - Data Type which describes the content type for the parameter value (Encapsulated in the [type])
 *  field).
 *
 * Since some parameters may be present in multiple actions, we're maintaining action parameters
 * as a separate and independent entity.
 *
 * @author Mehran Behnam
 */
@Entity
@Table(name = "parameters")
class Parameter(

    /**
     * The parameter name.
     */
    @Id
    val name: String,

    /**
     * The parameter type.
     */
    @Enumerated(EnumType.STRING)
    val type: ParameterType
) {

    /**
     * Parameters can be identified just be their names. That is, two parameters with the same name
     * should be considered as equal.
     */
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Parameter) return false

        return name == other.name
    }

    /**
     * A consistent hash code implementation with the [equals] method.
     */
    override fun hashCode(): Int {
        return name.hashCode()
    }

    override fun toString(): String {
        return "Parameter(name='$name', type=$type)"
    }
}

/**
 * Represents different possible parameter types.
 *
 * @author Mehran Behnam
 */
enum class ParameterType {

    /**
     * The corresponding parameter can contain a simple text value.
     */
    STRING,

    /**
     * The corresponding parameter can contain any number with any precision.
     */
    NUMBER,

    /**
     * The corresponding parameter represents a temporal value.
     */
    DATE_TIME,

    /**
     * The corresponding parameter contains a phone number string.
     */
    PHONE_NUMBER
}

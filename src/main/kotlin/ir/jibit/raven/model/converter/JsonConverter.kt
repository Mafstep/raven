package ir.jibit.raven.model.converter

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import javax.persistence.AttributeConverter

/**
 * [AttributeConverter] responsible for converting [Map]s (In JPA entity definitions) to
 * JSON-ified [String]s (In Database columns) and vice versa. For example, if your entity
 * contains the following map:
 * ```
 *     val mapping = mapOf("name" to "ali", "age" to "29")
 * ```
 * This converter would persist this map as the following JSON value:
 * ```
 *     {
 *         "name": "ali",
 *         "age": "29"
 *     }
 * ```
 *
 * @author Mehran Behnam
 */
class JsonConverter : AttributeConverter<Map<String, String>, String> {

    /**
     * Would be used for JSON serialization/de-serialization.
     */
    private val serializer = ObjectMapper()

    /**
     * Converts the entity [Map]s into JSON [String]s.
     */
    override fun convertToDatabaseColumn(attribute: Map<String, String>?): String = when (attribute) {
        null -> "{}"
        else -> serializer.writeValueAsString(attribute)
    }

    /**
     * Converts the persisted JSON string back to the original [Map].
     */
    override fun convertToEntityAttribute(dbData: String?): Map<String, String> = when (dbData) {
        null -> emptyMap()
        else -> serializer.readValue(dbData.toByteArray())
    }
}

package ir.jibit.raven.model.converter

import javax.persistence.AttributeConverter

/**
 * [AttributeConverter] responsible for converting [Set]s (In JPA entities) to Comma-Separated
 * values (In Database columns) and vice versa. For example, if a JPA entity contains a set like:
 * ```
 *     val sports = setOf("Football", "Basketball")
 * ```
 * Then the persisted CSV would be like:
 * ```
 *    Football,Basketball
 * ```
 *
 * @author Mehran Behnam
 */
class MultiValueFieldConverter : AttributeConverter<Set<String>, String> {

    /**
     * Converts the JPA bound [Set] to a CSV [String] to be persisted into DB.
     */
    override fun convertToDatabaseColumn(attribute: Set<String>?): String = when (attribute) {
        null -> ""
        else -> attribute.joinToString(separator = ",")
    }

    /**
     * Converts the persisted CSV [String] back into a [Set] of values.
     */
    override fun convertToEntityAttribute(dbData: String?): Set<String> = when {
        dbData == null -> emptySet()
        dbData.isBlank() -> emptySet()
        else -> dbData.split(",").toSet()
    }
}

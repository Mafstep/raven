package ir.jibit.raven.model

/**
 * Represents a phone number string.
 *
 * @author Mehran Behnam
 */
typealias PhoneNumber = String

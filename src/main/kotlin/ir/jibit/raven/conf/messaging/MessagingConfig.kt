package ir.jibit.raven.conf.messaging

import org.springframework.amqp.core.Queue
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/**
 * Responsible for configuring a few queues for publishing notification messages.
 *
 * @author Mehran Behnam
 */
@Configuration
class MessagingConfig {

    /**
     * Static-ish holder for queue names.
     */
    companion object {
        const val pushQueueName = "notifications.push"
        const val smsQueueName = "notifications.sms"
        const val emailQueueName = "notifications.email"
    }

    /**
     * Registers an AMQP queue for push notification messages.
     */
    @Bean
    fun pushQueue() = Queue(pushQueueName, true)

    /**
     * Registers an AMQP queue for SMS notifications.
     */
    @Bean
    fun smsQueue() = Queue(smsQueueName, true)

    /**
     * Registers an AMQP queue for Email notification.
     */
    @Bean
    fun emailQueue() = Queue(emailQueueName, true)
}

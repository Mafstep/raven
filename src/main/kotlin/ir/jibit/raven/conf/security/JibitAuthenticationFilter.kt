package ir.jibit.raven.conf.security

import org.springframework.http.HttpHeaders.AUTHORIZATION
import org.springframework.security.core.context.SecurityContextHolder
import javax.servlet.Filter
import javax.servlet.FilterChain
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest

/**
 * Simple filter responsible for populating the authentication request with the `Authorization`
 * header in the HTTP request.
 *
 * After populating the [SecurityContextHolder] with a particular authentication request type, registered
 * authentication providers can inspect the request and possibly process it.
 *
 * @author Mehran Behnam
 */
class JibitAuthenticationFilter : Filter {

    /**
     * The bearer token prefix!
     */
    private val bearer = "Bearer "

    /**
     * First off, it Extracts the `Bearer` token from the `Authorization` header. If such token exists, then
     * populates the auth request in the [SecurityContextHolder]. Otherwise, simply resumes the process.
     */
    override fun doFilter(request: ServletRequest, response: ServletResponse, chain: FilterChain) {
        val token = extractBearerTokenFromHeader(request)
        if (token != null) {
            SecurityContextHolder.getContext().authentication = AuthenticationRequest(token)
        }
        chain.doFilter(request, response)
    }

    /**
     * Extracts the `Bearer` token from the request header and returns it. Also,
     * returns `null` when either the header is missing or the header value does
     * not follow the `Bearer <token>` format.
     */
    private fun extractBearerTokenFromHeader(request: ServletRequest): String? {
        val httpRequest = request as HttpServletRequest
        val authorization = httpRequest.getHeader(AUTHORIZATION) ?: return null

        if (authorization.startsWith(bearer)) return authorization

        return null
    }
}

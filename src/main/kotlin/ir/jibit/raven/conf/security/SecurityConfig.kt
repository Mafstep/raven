package ir.jibit.raven.conf.security

import ir.jibit.raven.web.tokens
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.builders.WebSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy.STATELESS
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter

/**
 * Responsible for configuring the Security aspects of application.
 *
 * @author Mehran Behnam
 */
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
class SecurityConfig : WebSecurityConfigurerAdapter() {

    override fun configure(web: WebSecurity) {
        web.ignoring()
            .requestMatchers(JibitAuthTokenMatcher())
    }

    override fun configure(http: HttpSecurity) {
        // @formatter:off
        http
                .addFilterBefore(JibitAuthenticationFilter(), UsernamePasswordAuthenticationFilter::class.java)
                .sessionManagement()
                    .sessionCreationPolicy(STATELESS)
                .and()
                .authorizeRequests()
                    .mvcMatchers(tokens).authenticated()
                    .anyRequest().permitAll()
                .and()
                .csrf()
                    .disable()
        // @formatter:on
    }
}

package ir.jibit.raven.conf.security

import ir.jibit.raven.web.tokens
import org.springframework.security.web.util.matcher.RequestMatcher
import javax.servlet.http.HttpServletRequest

/**
 * A request matcher to match with all endpoints that don't need our TServer based
 * authentication.
 *
 * @author Mehran Behnam
 */
class JibitAuthTokenMatcher : RequestMatcher {

    /**
     * Currently all endpoints but `/v1/tokens` should be openly accessible.
     */
    override fun matches(request: HttpServletRequest) = !request.requestURI.contains(tokens)
}

package ir.jibit.raven.conf.security

import ir.jibit.raven.model.user.User
import org.springframework.security.core.Authentication
import org.springframework.security.core.authority.SimpleGrantedAuthority

/**
 * Base class for all authentication tokens.
 *
 * @author Mehran Behnam
 */
sealed class JibitToken(val token: String) : Authentication {

    /**
     * Returning a dummy role.
     */
    override fun getAuthorities() = listOf(SimpleGrantedAuthority("ROLE_USER"))

    /**
     * We consider the given JWT tokens as the credentials.
     */
    override fun getCredentials() = token

    /**
     * Since the [getName] represents the phoneNumber number for the user, we're wrapping the
     * phoneNumber number inside a [User] and returning it as our user details.
     */
    override fun getDetails(): Any {
        return User(name)
    }

    /**
     * Same as the [getDetails].
     */
    override fun getPrincipal(): Any {
        return User(name)
    }

    /**
     * No-Op implementation.
     */
    override fun setAuthenticated(isAuthenticated: Boolean) {}
}

/**
 * Encapsulates the required parameters to submit an authentication request. This token
 * represents a request for authentication and the token is not an authenticated one.
 *
 * @author Mehran Behnam
 */
class AuthenticationRequest(token: String) : JibitToken(token) {

    /**
     * A dummy username for the request.
     */
    override fun getName() = "Not Authenticated Yet!"

    /**
     * The request is not authenticated yet.
     */
    override fun isAuthenticated() = false
}

/**
 * Encapsulates the authenticated user info after a successful authentication process.
 *
 * @author Mehran Behnam
 */
class AuthenticatedRequest(val user: String) : JibitToken("Authenticated!") {

    /**
     * The username, i.e. The phoneNumber number.
     */
    override fun getName() = user

    /**
     * This token represents a successful authentication request.
     */
    override fun isAuthenticated() = true
}

package ir.jibit.raven.conf.security

import ir.jibit.raven.model.user.User
import ir.jibit.raven.service.user.UserService
import ir.jibit.raven.util.logger
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.AuthenticationServiceException
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Component

/**
 * An authentication provider responsible for handling authentication requests with the
 * [AuthenticationRequest] token types.
 *
 * @author Mehran Behnam
 */
@Component
class JibitAuthenticationProvider(private val userService: UserService) : AuthenticationProvider {

    /**
     * Logger.
     */
    private val log = logger<JibitAuthenticationProvider>()

    /**
     * Tries to verify the given token using our identity service (Currently IranPay).
     * If the given token corresponds to a valid user, then the authentication request
     * would be marked as a successful one. Otherwise, the request remain un-authenticated.
     */
    override fun authenticate(authentication: Authentication): Authentication {
        val request = authentication as AuthenticationRequest
        log.debug("About to verify an authentication token: {}", request.credentials)

        val user: User?
        try {
            user = userService.findByToken(request.token)
            log.debug("Successfully verified the `{}` token: {}", request.credentials, user)
        } catch (e: Exception) {
            log.debug("Failed to process the authentication request", e)
            throw AuthenticationServiceException(e.message, e)
        }

        return user?.let { AuthenticatedRequest(it.phoneNumber) }
            ?: throw BadCredentialsException("Invalid token: $authentication")
    }

    /**
     * This implementation can only support the [AuthenticationRequest] authentication requests.
     */
    override fun supports(authentication: Class<*>?) = authentication == AuthenticationRequest::class.java
}

package ir.jibit.raven.conf.http

import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.client.RestTemplate
import java.time.Duration

/**
 * Registers an HTTP client to call other REST services.
 *
 * @author Mehran Behnam
 */
@Configuration
class ClientConfig {

    /**
     * The to-be-registered HTTP client. This HTTP client has a connect and read timeout of one second.
     *
     * TODO Timout values can be configurable!
     */
    @Bean
    fun restTemplate(restTemplateBuilder: RestTemplateBuilder): RestTemplate {
        return restTemplateBuilder
            .setConnectTimeout(Duration.ofSeconds(1))
            .setReadTimeout(Duration.ofSeconds(1))
            .build()
    }
}

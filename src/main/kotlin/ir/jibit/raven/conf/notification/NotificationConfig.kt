package ir.jibit.raven.conf.notification

import ir.jibit.raven.conf.messaging.MessagingConfig.Companion.emailQueueName
import ir.jibit.raven.conf.messaging.MessagingConfig.Companion.pushQueueName
import ir.jibit.raven.conf.messaging.MessagingConfig.Companion.smsQueueName
import ir.jibit.raven.model.notification.EmailNotification
import ir.jibit.raven.model.notification.PushNotification
import ir.jibit.raven.model.notification.SmsNotification
import ir.jibit.raven.notification.NotificationProcessor
import ir.jibit.raven.notification.RabbitMqExchangeNotificationProcessor
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/**
 * Configures different notification processors for different notification types.
 *
 * @author Mehran Behnam
 */
@Configuration
class NotificationConfig(private val rabbitTemplate: RabbitTemplate) {

    /**
     * Registers a dedicated notification processor for Push notifications.
     */
    @Bean
    fun pushNotificationProcessor(): NotificationProcessor =
        RabbitMqExchangeNotificationProcessor(rabbitTemplate, pushQueueName, PushNotification::class)

    /**
     * Registers a dedicated notification processor for SMS notifications.
     */
    @Bean
    fun smsNotificationProcessor(): NotificationProcessor =
        RabbitMqExchangeNotificationProcessor(rabbitTemplate, smsQueueName, SmsNotification::class)

    /**
     * Registers a dedicated notification processor for Email notifications.
     */
    @Bean
    fun emailNotificationProcessor(): NotificationProcessor =
        RabbitMqExchangeNotificationProcessor(rabbitTemplate, emailQueueName, EmailNotification::class)
}

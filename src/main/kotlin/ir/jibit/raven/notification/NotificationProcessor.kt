package ir.jibit.raven.notification

import ir.jibit.raven.model.notification.Notification

/**
 * Defines a contract to process a notification. Different notification types could be
 * handled by different [NotificationProcessor]s. The [canProcess] method determines
 * whether a particular [NotificationProcessor] can process a given [Notification]
 * or not.
 *
 * @author Mehran Behnam
 */
interface NotificationProcessor {

    /**
     * Performs the actual mechanics of processing a notification. Should call this method
     * iff the call to [canProcess] returns true for the same notification.
     *
     * @throws NotificationProcessingFailureException When we fail to process the notification.
     */
    fun process(notification: Notification)

    /**
     * Determines whether or not this particular implementation can process the given [Notification]
     * or not.
     */
    fun canProcess(notification: Notification): Boolean
}

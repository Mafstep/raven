package ir.jibit.raven.notification

import ir.jibit.raven.model.notification.Notification
import me.alidg.errors.annotation.ExceptionMapping
import org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR

/**
 * Raised when we fail to process a notification:
 *  - [cause] The actual cause of the failure.
 *  - [notification] The notification we've failed to send.
 *
 * @author Mehran Behnam
 */
@ExceptionMapping(errorCode = "notification.failed_processing", statusCode = INTERNAL_SERVER_ERROR)
class NotificationProcessingFailureException(override val cause: Throwable, val notification: Notification)
    : RuntimeException()

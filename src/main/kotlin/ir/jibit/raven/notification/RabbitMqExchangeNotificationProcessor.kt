package ir.jibit.raven.notification

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import ir.jibit.raven.model.notification.Notification
import ir.jibit.raven.util.logger
import org.springframework.amqp.core.Message
import org.springframework.amqp.core.MessageProperties
import org.springframework.amqp.rabbit.core.RabbitTemplate
import kotlin.reflect.KClass

/**
 * Process the given notifications by sending them to RabbitMQ's default exchange
 * with the [queueName] as the routing key. Simply put, this implementation delegates
 * the actual notification processing logic to those who subscribing to the [queueName]:
 *  - [rabbitTemplate] Enables us to communicate with a RabbitMQ broker.
 *  - [queueName] The queue name to send the notification to.
 *  - [canProcess] Determines the notification type that the implementation can handle.
 *
 * @author Mehran Behnam
 */
class RabbitMqExchangeNotificationProcessor(private val rabbitTemplate: RabbitTemplate,
                                            private val queueName: String,
                                            private val canProcess: KClass<out Notification>)
    : NotificationProcessor {

    private val log = logger<RabbitMqExchangeNotificationProcessor>()

    /**
     * Would be used to serialize the notifications as JSON.
     */
    private val serializer = ObjectMapper().registerModule(JavaTimeModule())

    /**
     * Sends the serialized version of the given [notification] to a RabbitMQ queue named [queueName].
     */
    override fun process(notification: Notification) {
        try {
            val message = serializer.writeValueAsBytes(notification)
            rabbitTemplate.send(queueName, Message(message, notification.messageProperties()))
        } catch (ex: Throwable) {
            log.error("Failed to send a notification", ex)
            throw NotificationProcessingFailureException(ex, notification)
        }
    }

    override fun canProcess(notification: Notification) = notification::class == canProcess
}

private fun Notification.messageProperties(): MessageProperties {
    val messageProperties = MessageProperties()
    if (ttl > 0) messageProperties.expiration = "${ttl * 1000}"

    return messageProperties
}

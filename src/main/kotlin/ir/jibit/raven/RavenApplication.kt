package ir.jibit.raven

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class RavenApplication

/**
 * Application main entry.
 */
fun main(args: Array<String>) {
    runApplication<RavenApplication>(*args)
}
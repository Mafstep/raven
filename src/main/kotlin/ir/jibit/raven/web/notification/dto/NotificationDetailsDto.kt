package ir.jibit.raven.web.notification.dto

import ir.jibit.raven.model.notification.EmailNotification
import ir.jibit.raven.model.notification.Notification
import ir.jibit.raven.model.notification.PushNotification
import ir.jibit.raven.model.notification.SmsNotification
import java.time.Instant

/**
 * DTO representing a summary view for a notification.
 *
 * @author Mehran Behnam
 */
class NotificationSummaryDto(

    /**
     * The notification id.
     */
    val id: String,

    /**
     * The notification processing state.
     */
    val status: String,

    /**
     * When the notification got submitted?
     */
    val whenSubmitted: Instant,

    /**
     * Type of the notification.
     */
    val type: String
)

/**
 * DTO representing a detailed view of a notification.
 *
 * @author Mehran Behnam
 */
class DetailedNotificationDto(

    /**
     * Notification id.
     */
    val id: String,

    /**
     * Key-value pairs to customize each notifications.
     */
    val data: Map<String, String>,

    /**
     * Time to live for notification.
     */
    val ttl: Int,

    /**
     * The notification processing state.
     */
    val status: String,

    /**
     * Processing log (either failure or successful) for a notification.
     */
    val processLog: String?,

    /**
     * When the notification got submitted?
     */
    val whenSubmitted: Instant,

    /**
     * Represents the notification type.
     */
    val type: String,

    /**
     * Recipients of a SMS or Email notification.
     */
    var recipients: Set<String>? = null,

    /**
     * An email notification subject.
     */
    var subject: String? = null,

    /**
     * Can be email notification or push notification body.
     */
    var body: String? = null,

    /**
     * Title for a push notification.
     */
    var title: String? = null,

    /**
     * Tokens for a push notifications.
     */
    var recipientTokens: Set<String>? = null,

    /**
     * Push notification topic.
     */
    var topic: String? = null,

    /**
     * SMS notification message.
     */
    var message: String? = null
)

/**
 * Converts the receiving [Notification] to its corresponding [NotificationSummaryDto].
 *
 * @author Mehran Behnam
 */
fun Notification.toSummaryDto() = NotificationSummaryDto(
    id.toString(),
    notificationState.name,
    whenSubmitted,
    getNotificationType()
)

/**
 * Converts the receiving [Notification] to its corresponding detailed DTO view. Please note
 * that based on the notification type we may populate some extra fields.
 *
 * @author Mehran Behnam
 */
fun Notification.toDetailedDto(): DetailedNotificationDto {
    val dto = DetailedNotificationDto(id.toString(), data, ttl, notificationState.name, processLog, whenSubmitted,
        getNotificationType())

    when (this) {
        is PushNotification -> populatePushProperties(dto)
        is SmsNotification -> populateSmsProperties(dto)
        is EmailNotification -> populateEmailProperties(dto)
    }

    return dto
}

private fun PushNotification.populatePushProperties(dto: DetailedNotificationDto) {
    dto.title = title
    dto.body = body
    dto.recipientTokens = recipientTokens
    dto.topic = topic
}

private fun SmsNotification.populateSmsProperties(dto: DetailedNotificationDto) {
    dto.recipients = recipients
    dto.message = message
}

private fun EmailNotification.populateEmailProperties(dto: DetailedNotificationDto) {
    dto.recipients = recipients
    dto.subject = subject
    dto.body = body
}

private fun Notification.getNotificationType(): String = when (this) {
    is PushNotification -> "PUSH"
    is SmsNotification -> "SMS"
    is EmailNotification -> "EMAIL"
    else -> "n/a"
}

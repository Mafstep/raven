package ir.jibit.raven.web.notification

import ir.jibit.raven.service.notification.NotificationService
import ir.jibit.raven.service.token.RegistrationTokenService
import ir.jibit.raven.web.Paginated
import ir.jibit.raven.web.notification.dto.*
import ir.jibit.raven.web.notifications
import ir.jibit.raven.web.validator.ValidationSeq
import org.springframework.data.domain.Pageable
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import java.lang.Exception
import java.util.*

/**
 * Provides a REST API to manage notifications.
 *
 * @author Mehran Behnam
 */
@RestController
@RequestMapping(notifications)
class NotificationController(private val notificationService: NotificationService,
                             private val registrationTokenService: RegistrationTokenService) {

    /**
     * Listing a paginated collection of recently submitted notifications. Possible responses are:
     *  - 200 OK: When we successfully managed to retrieve the notifications.
     *  - 403 Forbidden: When the client does not possess the required permissions to call this service.
     *  - 500 Internal Server Error: When the server failed to handle the request.
     */
    @GetMapping
    fun listNotifications(pageable: Pageable) =
        Paginated(notificationService.latestNotifications(pageable).map { it.toSummaryDto() })

    /**
     * Submits a notification for further processing. Possible responses are:
     *  - 200 OK: Notification successfully submitted.
     *  - 400 Bad Request: The request body is invalid.
     *  - 403 Forbidden: Client does not possess the required permissions to use this service.
     *  - 500 Internal Server Error: Server failed to handle the request.
     *
     * Please note that we're passing a lambda function to convert User Identifiers (currently phone numbers)
     * to Device Tokens.
     */
    @PostMapping
    fun submitNotification(@RequestBody @Validated(ValidationSeq::class) dto: NewNotificationDto): SubmittedNotificationDto {
        val userToTokenConverter = { users: Set<String> ->
            registrationTokenService.getDeviceTokens(users).map { it.token }.toSet()
        }
        val notification = dto.toEntity(userToTokenConverter)

        notificationService.sendNotification(notification)
        return SubmittedNotificationDto(notification.id.toString(), notification.notificationState.name)
    }

    /**
     * Returns a detailed view for the requested notification, identified by the given [id]. Possible responses
     * are:
     *  - 200 OK: Along with the detailed view of the notification.
     *  - 403 Forbidden: When the client does not possess the required permissions to call this service.
     *  - 404 Not Found: When the requested notification does not exist.
     *  - 500 Internal Server Error: When the server failed to handle the request.
     */
    @GetMapping("/{id}")
    fun getNotification(@PathVariable id: String): DetailedNotificationDto {
        val uuid = try { UUID.fromString(id) } catch (e: Exception) { throw NotificationNotFoundException() }
        val notificationDto = notificationService.getNotification(uuid)?.toDetailedDto()

        return notificationDto ?: throw NotificationNotFoundException()
    }
}

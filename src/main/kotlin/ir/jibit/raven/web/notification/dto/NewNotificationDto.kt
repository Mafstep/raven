package ir.jibit.raven.web.notification.dto

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonSubTypes.Type
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.annotation.JsonTypeInfo.As.PROPERTY
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id.NAME
import ir.jibit.raven.model.notification.EmailNotification
import ir.jibit.raven.model.notification.Notification
import ir.jibit.raven.model.notification.PushNotification
import ir.jibit.raven.model.notification.SmsNotification
import ir.jibit.raven.util.isCompletelyEmpty
import ir.jibit.raven.web.validator.Presence
import ir.jibit.raven.web.validator.Validity
import org.springframework.core.MethodParameter
import org.springframework.validation.BeanPropertyBindingResult
import org.springframework.web.bind.MethodArgumentNotValidException
import javax.validation.constraints.AssertTrue
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

/**
 * All possible notification types.
 */
private val notificationTypes = setOf("PUSH", "SMS", "EMAIL")

/**
 * Regex pattern for phone numbers.
 */
private val phoneNumberPattern = Regex("""^\+?\d+$""")

/**
 * A fairly complex piece of Regex to validate emails.
 *
 * Shamelessly copy-pasted from StackOverflow.
 */
private val emailAddressPattern = Regex("""(?:[a-z0-9!#${'$'}%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#${'$'}%&'*+/=?^_`{|}~-]+)*|
    |"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]
    |*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5
    |[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\
    |[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])""".trimMargin())

/**
 * Base class for all notification DTOs. The [type] field determines the notification type and
 * helps the Jackson to instantiate the appropriate subclass based on the provided metadata by
 * annotations.
 *
 * @author Mehran Behnam
 */
@JsonSubTypes(
    Type(value = NewPushNotificationDto::class, name = "PUSH"),
    Type(value = NewSmsNotificationDto::class, name = "SMS"),
    Type(value = NewEmailNotificationDto::class, name = "EMAIL")
)
@JsonTypeInfo(use = NAME, include = PROPERTY, property = "type", defaultImpl = NewNotificationDto::class, visible = true)
open class NewNotificationDto(

    /**
     * The metadata-ish key-value pairs which can customize each notification type in its unique way.
     */
    var data: Map<String, String>? = null,

    /**
     * How long (In seconds) we can keep the message?
     */
    @get:NotNull(message = "notification.ttl.required", groups = [Presence::class])
    var ttl: Int? = null,

    /**
     * Determines the notification type.
     */
    @get:NotBlank(message = "notification.type.required", groups = [Presence::class])
    var type: String? = null
) {

    /**
     * Responsible for converting the current DTO to its corresponding entity.
     */
    open fun toEntity(mapper: (Set<String>) -> Set<String>): Notification = Notification()

    /**
     * Make sure that the given notification type is valid.
     */
    @AssertTrue(message = "notification.type.invalid", groups = [Validity::class])
    fun isValidType(): Boolean = notificationTypes.contains(type)
}

/**
 * DTO encapsulating a web request parameters for push notifications.
 *
 * @author Mehran Behnam
 */

class NewPushNotificationDto(

    /**
     * The device tokens.
     */
    var recipientTokens: Set<String>? = null,

    /**
     * Collection of users as the push notification recipient.
     */
    var users: Set<String>? = null,

    /**
     * The destination topic.
     */
    var topic: String? = null,

    /**
     * The notification title.
     */
    @get:NotBlank(message = "notification.push.title.required", groups = [Presence::class])
    var title: String? = null,

    /**
     * The notification body.
     */
    @get:NotBlank(message = "notification.push.body.required", groups = [Presence::class])
    var body: String? = null

) : NewNotificationDto() {

    /**
     * Make sure at least one destination information is attached to the given push notification. Simply put:
     * ```
     *     all destinations types can't be missing =
     *     !(topicIsNull and tokensAreNull and usersAreNull) = !topicIsNull or !tokensAreNull or !usersAreNull
     * ```
     */
    @get:AssertTrue(message = "notification.push.destination.required", groups = [Validity::class])
    val hasAtLeastOneDestination: Boolean by lazy {
        !topic.isNullOrBlank() || !recipientTokens.isCompletelyEmpty() || !users.isCompletelyEmpty()
    }

    /**
     * Converts the receiving DTO to its corresponding entity,
     */
    override fun toEntity(mapper: (Set<String>) -> (Set<String>)): Notification {
        return PushNotification(title!!, body!!, getTokens(mapper), topic).updateWith(this)
    }

    private fun getTokens(mapper: (Set<String>) -> (Set<String>)): Set<String>? {
        if (!users.isCompletelyEmpty()) {
            val mappedTokens = mapper(users!!)
            if (!mappedTokens.isCompletelyEmpty()) return mappedTokens
        }

        failIfNoOtherDestinationIsProvided()

        return recipientTokens
    }

    private fun failIfNoOtherDestinationIsProvided() {
        if (topic.isNullOrBlank() && recipientTokens.isCompletelyEmpty()) {
            val bindingResult = getBindingResult()
            val parameter = getMethodParameter()

            throw MethodArgumentNotValidException(parameter, bindingResult)
        }
    }

    private fun getMethodParameter(): MethodParameter {
        return MethodParameter(javaClass.methods[1], -1)
    }

    private fun getBindingResult(): BeanPropertyBindingResult {
        val errorCode = "notification.push.destination.required"
        val bindingResult = BeanPropertyBindingResult(this, "newPushNotificationDto")
        bindingResult.reject(errorCode, errorCode)

        return bindingResult
    }
}

/**
 * DTO encapsulating SMS notification details.
 *
 * @author Mehran Behnam
 */
class NewSmsNotificationDto(

    /**
     * Recipient phone numbers.
     */
    @get:NotEmpty(message = "notification.sms.recipients.required", groups = [Presence::class])
    var recipients: Set<String>? = null,

    /**
     * The SMS text message.
     */
    @get:NotBlank(message = "notification.sms.message.required", groups = [Presence::class])
    var message: String? = null

) : NewNotificationDto() {

    /**
     * Make sure that all the given phone numbers, i.e. [recipients], are valid.
     */
    @get:AssertTrue(message = "notification.sms.recipients.invalid", groups = [Validity::class])
    val arePhoneNumbersValid: Boolean by lazy {
        recipients!!.all { phoneNumberPattern.matches(it) }
    }

    /**
     * Converts the receiving DTO to its corresponding entity,
     */
    override fun toEntity(mapper: (Set<String>) -> (Set<String>)) = SmsNotification(recipients!!, message!!).updateWith(this)
}

/**
 * DTO encapsulating email notification details.
 *
 * @author Mehran Behnam
 */
class NewEmailNotificationDto(

    /**
     * Recipient email addresses.
     */
    @get:NotEmpty(message = "notification.email.recipients.required", groups = [Presence::class])
    var recipients: Set<String>? = null,

    /**
     * Email subject.
     */
    @get:NotBlank(message = "notification.email.subject.required", groups = [Presence::class])
    var subject: String? = null,

    /**
     * Email body.
     */
    @get:NotBlank(message = "notification.email.body.required", groups = [Presence::class])
    var body: String? = null

) : NewNotificationDto() {

    /**
     * Make sure that the email addresses are valid.
     */
    @get:AssertTrue(message = "notification.email.recipients.invalid", groups = [Validity::class])
    val areEmailAddressValid: Boolean by lazy {
        recipients!!.all { emailAddressPattern.matches(it) }
    }

    /**
     * Converts the receiving DTO to its corresponding entity,
     */
    override fun toEntity(mapper: (Set<String>) -> (Set<String>)) = EmailNotification(recipients!!, subject!!, body!!).updateWith(this)
}

/**
 * Updates the receiving [Notification] with the given DTO details.
 */
fun Notification.updateWith(dto: NewNotificationDto) = apply {
    ttl = dto.ttl!!
    data = dto.data ?: emptyMap()
}

package ir.jibit.raven.web.notification.dto

/**
 * Encapsulates a minimal response for a successful notification submission.
 *
 * @author Mehran Behnam
 */
data class SubmittedNotificationDto(val id: String, val status: String)

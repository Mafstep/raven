package ir.jibit.raven.web.notification

import me.alidg.errors.annotation.ExceptionMapping
import org.springframework.http.HttpStatus
import java.lang.RuntimeException

/**
 * Raised when we couldn't found the requested notification.
 *
 * @author Mehran Behnam
 */
@ExceptionMapping(errorCode = "notification.not_found", statusCode = HttpStatus.NOT_FOUND)
class NotificationNotFoundException : RuntimeException()

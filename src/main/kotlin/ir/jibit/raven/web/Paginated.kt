package ir.jibit.raven.web

import org.springframework.data.domain.Slice

/**
 * DTO to wrap paginated collection of elements.
 *
 * @author Mehran Behnam
 */
class Paginated<T>(

    /**
     * The current 1-index page number.
     */
    val pageNumber: Int,

    /**
     * The size of current page.
     */
    val size: Int,

    /**
     * Total number of elements in the current page.
     */
    val numberOfElements: Int,

    /**
     * Can this page be followed by another page after it?
     */
    val hasNext: Boolean,

    /**
     * Is there a previous page for this page?
     */
    val hasPrevious: Boolean,

    /**
     * The actual paginated elements.
     */
    val elements: List<T>
) {

    /**
     * Constructor responsible for creating a [Paginated] from the given [Slice] of elements.
     */
    constructor(slice: Slice<T>) :
        this(slice.number + 1, slice.size, slice.numberOfElements, slice.hasNext(), slice.hasPrevious(), slice.content)
}

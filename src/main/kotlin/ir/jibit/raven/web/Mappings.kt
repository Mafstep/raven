package ir.jibit.raven.web

// API Version 1
const val v1 = "/v1"

// Actions API
const val actions = "$v1/actions"

// Parameters API
const val parameters = "$v1/parameters"

// Notifications API
const val notifications = "$v1/notifications"

// Token API
const val tokens = "$v1/tokens"
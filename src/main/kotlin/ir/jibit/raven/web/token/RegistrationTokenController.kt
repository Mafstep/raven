package ir.jibit.raven.web.token

import ir.jibit.raven.service.token.RegistrationTokenService
import ir.jibit.raven.service.user.UserService
import ir.jibit.raven.web.token.dto.NewRegistrationTokenDto
import ir.jibit.raven.web.token.dto.toEntity
import ir.jibit.raven.web.tokens
import ir.jibit.raven.web.validator.ValidationSeq
import org.springframework.http.HttpStatus
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*

/**
 * Provides a REST API to manage token parameters:
 * - [registrationTokenService] Will help us to manage user tokens.
 * - [userService] To find the current authenticated user.
 *
 * @author Nazila Akbari
 */
@RestController
@RequestMapping(tokens)
class RegistrationTokenController(private val registrationTokenService: RegistrationTokenService,
                                  private val userService: UserService) {

    /**
     * Registers a device token for the current authenticated user. Possible responses are:
     *  - 204 No Content: The token successfully registered for the user.
     *  - 400 Bad Request: The given parameters are invalid.
     *  - 403 Forbidden: The client does not posses the required permissions.
     *  - 500 Internal Server Error: When we failed to process the request.
     *
     * **Note:** Only authenticated users can call this service.
     */
    @PostMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun registerToken(@RequestBody @Validated(ValidationSeq::class) dto: NewRegistrationTokenDto) {
        val currentAuthenticatedUser = userService.currentAuthenticatedUser()
        // We're sure the user is authenticated.
        val entity = dto.toEntity(currentAuthenticatedUser!!.phoneNumber)

        registrationTokenService.registerToken(entity)
    }
}

package ir.jibit.raven.web.token.dto

import ir.jibit.raven.model.token.RegistrationToken
import ir.jibit.raven.model.token.TokenType
import ir.jibit.raven.web.validator.Presence
import ir.jibit.raven.web.validator.Validity
import javax.validation.constraints.AssertTrue
import javax.validation.constraints.NotBlank

/**
 * All valid token types.
 */
private val tokenTypes = setOf("IOS", "ANDROID", "WEB")

/**
 * DTO encapsulating a web request parameters for device token registration.
 *
 * @author Nazila Akbari
 */
class NewRegistrationTokenDto(

    /**
     * The device token.
     */
    @get:NotBlank(groups = [Presence::class], message = "token.required")
    val token: String? = null,

    /**
     * Determines the target device for the token. Currently we only support IOS, ANDROID and WEB targets.
     */
    @get:NotBlank(groups = [Presence::class], message = "token.type.required")
    val tokenType: String? = null,

    /**
     * The client app version.
     */
    @get:NotBlank(groups = [Presence::class], message = "app.version.required")
    val appVersion: String? = null,

    /**
     * Might encapsulate more metadata about registered token such as OS version or device name.
     */
    val metadata: Map<String, String>? = emptyMap()
) {

    /**
     * Makes sure that the given token type is valid.
     */
    @AssertTrue(message = "token.type.invalid", groups = [Validity::class])
    fun isValidType(): Boolean = tokenTypes.contains(tokenType)
}

fun NewRegistrationTokenDto.toEntity(phoneNumber: String): RegistrationToken = RegistrationToken(
    user = phoneNumber,
    token = token!!,
    tokenType = TokenType.valueOf(tokenType!!),
    appVersion = appVersion!!,
    metadata = metadata
)

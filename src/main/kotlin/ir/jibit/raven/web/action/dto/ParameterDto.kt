package ir.jibit.raven.web.action.dto

import ir.jibit.raven.model.action.Parameter
import ir.jibit.raven.model.action.ParameterType
import ir.jibit.raven.web.validator.Presence
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

/**
 * DTO encapsulating an action parameter. This DTO will be used for both creating an action parameter
 * and representing a detailed view of an action parameter.
 *
 * @author Mehran Behnam
 */
class ParameterDto(

    /**
     * The action parameter name, which is required when we're creating the parameter.
     */
    @get:NotBlank(groups = [Presence::class], message = "parameter.name.required")
    val name: String? = null,

    /**
     * The action parameter type which is required when we're creating the action parameter.
     */
    @get:NotNull(groups = [Presence::class], message = "parameter.type.required")
    val type: ParameterType? = null
)

/**
 * Converts the receiving DTO to its corresponding entity.
 *
 * @author Mehran Behnam
 */
fun ParameterDto.toEntity() = Parameter(name!!, type!!)

/**
 * Converts the receiving entity to its corresponding DTO.
 *
 * @author Mehran Behnam
 */
fun Parameter.toDto() = ParameterDto(name, type)

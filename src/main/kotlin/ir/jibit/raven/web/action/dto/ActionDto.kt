package ir.jibit.raven.web.action.dto

import ir.jibit.raven.model.action.Action
import ir.jibit.raven.model.notification.NotificationType

/**
 * DTO encapsulating details for an action.
 *
 * @author Mehran Behnam
 */
class ActionDto(

    /**
     * Action name.
     */
    val name: String,

    /**
     * The action parameters.
     */
    val parameters: List<ParameterDto>,

    /**
     * The action rules.
     */
    val actionRules: List<ActionRuleDto>
)

/**
 * DTO encapsulating the action rule details.
 *
 * @author Mehran Behnam
 */
class ActionRuleDto(

    /**
     * The primary notification type.
     */
    val notificationType: NotificationType,

    /**
     * The fallback notification type.
     */
    val fallbackFor: NotificationType?
)

/**
 * Converts the receiving [Action] entity to its corresponding DTO.
 *
 * @author Mehran Behnam
 */
fun Action.toDto() = ActionDto(
    name,
    parameters.map { ParameterDto(it.name, it.type) },
    actionRules.map { ActionRuleDto(it.notificationType, it.fallbackFor) }
)

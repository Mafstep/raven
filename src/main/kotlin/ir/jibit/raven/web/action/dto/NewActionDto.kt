package ir.jibit.raven.web.action.dto

import ir.jibit.raven.model.action.Action
import ir.jibit.raven.model.action.ActionRule
import ir.jibit.raven.model.action.Parameter
import ir.jibit.raven.model.action.ParameterType
import ir.jibit.raven.model.notification.NotificationType
import ir.jibit.raven.web.validator.Presence
import ir.jibit.raven.web.validator.Validity
import javax.validation.Valid
import javax.validation.constraints.AssertTrue
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

/**
 * DTO encapsulating the required parameters and validations to create an action.
 *
 * @author Mehran Behnam
 */
class NewActionDto(

    /**
     * The action name which is required.
     */
    @get:NotBlank(groups = [Presence::class], message = "action.name.required")
    val name: String? = null,

    /**
     * Required parameters to create the action. An action can be parameter-less, but
     * when there is at least one parameter, it should not be blank.
     */
    val parameters: Set<String>? = null,

    /**
     * List of required action rules to create the action. At least one action rule should
     * be present and action rule fallbacks should be well-defined.
     */
    @get:Valid
    @get:NotNull(groups = [Presence::class], message = "action.rules.required")
    @get:NotEmpty(groups = [Validity::class], message = "action.rules.required")
    val actionRules: Set<NewActionRuleDto>? = null
) {

    /**
     * Ensures that the [parameters] is either `null` or contain no blank string.
     */
    @AssertTrue(groups = [Validity::class], message = "action.parameters.invalid")
    fun hasValidParameters() = parameters?.all { it.isNotBlank() } ?: true
}

/**
 * DTO encapsulating a new action rule which contains a primary notification type and a fallback.
 *
 * @author Mehran Behnam
 */
class NewActionRuleDto(

    /**
     * The primary notification type which is required.
     */
    @get:NotNull(groups = [Presence::class], message = "action.rules.notification.required")
    val notificationType: NotificationType? = null,

    /**
     * If provided, the [notificationType] will be used when the [fallbackFor] notification has
     * been failed.
     */
    val fallbackFor: NotificationType? = null
)

/**
 * Converts the receiving [NewActionDto] to its corresponding entity.
 */
fun NewActionDto.toEntity(): Action {
    val parameters = getParameters(parameters)
    val rules = getRules(actionRules)
    val name = getActionName(this)

    return Action(name, parameters, rules)
}

/**
 * Converts the given collection of [rules] to a collection of [ActionRule]s. The [IllegalStateException]
 * should not happen in real life since the non-nullity of the `actionRules` is checked before with
 * Bean Validation constraints.
 *
 * @throws IllegalStateException In post-apocalyptic scenarios.
 */
private fun getRules(rules: Set<NewActionRuleDto>?) =
    rules?.map { toActionRule(it) }?.toSet()
        ?: throw IllegalStateException("Action rules shouldn't be null")

/**
 * Converts the given [NewActionRuleDto] to its corresponding entity form which is [ActionRule]. The
 * [IllegalStateException] should not happen in real life since the non-nullity of the `notificationType`
 * is checked before with Bean Validation constraints.
 *
 * @throws IllegalStateException In post-apocalyptic scenarios.
 */
private fun toActionRule(dto: NewActionRuleDto): ActionRule {
    val notificationType = dto.notificationType ?: throw IllegalStateException("Notification shouldn't be null")
    return ActionRule(notificationType, dto.fallbackFor)
}

/**
 * Converts the given collection of parameter names to collection of [Parameter]s. We're passing a dummy
 * parameter type to the [Parameter] constructor since the parameter name would suffice for loading the
 * [Parameter] from DB.
 */
private fun getParameters(dto: Set<String>?): Set<Parameter> {
    val dummyType = ParameterType.STRING
    return dto?.map { Parameter(it, dummyType) }?.toSet() ?: emptySet()
}


private fun getActionName(dto: NewActionDto) =
    dto.name ?: throw IllegalStateException("Name shouldn't be null")

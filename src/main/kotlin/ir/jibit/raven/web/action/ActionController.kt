package ir.jibit.raven.web.action

import ir.jibit.raven.service.action.ActionNotFoundException
import ir.jibit.raven.service.action.ActionService
import ir.jibit.raven.web.Paginated
import ir.jibit.raven.web.action.dto.ActionDto
import ir.jibit.raven.web.action.dto.NewActionDto
import ir.jibit.raven.web.action.dto.toDto
import ir.jibit.raven.web.action.dto.toEntity
import ir.jibit.raven.web.actions
import ir.jibit.raven.web.validator.ValidationSeq
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus.NO_CONTENT
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*

/**
 * Provides a REST API to manage actions:
 *  - [actionService] The service responsible for creating and maintaining actions.
 *
 * @author Mehran Behnam
 */
@RestController
@RequestMapping(actions)
class ActionController(private val actionService: ActionService) {

    /**
     * REST endpoint responsible for creating an action. Common responses are:
     *  - 204 No Content: When the action has been created successfully.
     *  - 400 Bad Request: When given parameters, in the request body, are invalid.
     *  - 403 Forbidden: Client does not posses the required permissions.
     *  - 404 Not Found: When one of the parameters referenced in the action details
     *    can't be found.
     *  - 500 Internal Server Error: Server, for whatever reason, failed to handle
     *    the request.
     */
    @PostMapping
    @ResponseStatus(NO_CONTENT)
    fun createAction(@RequestBody @Validated(ValidationSeq::class) newAction: NewActionDto) {
        actionService.createAction(newAction.toEntity())
    }

    /**
     * Returns a paginated list of actions. Possible responses are:
     *  - 200 OK: When everything is OK.
     *  - 403 Forbidden: Client does not posses the required permissions.
     *  - 500 Internal Server Error: Server, for whatever reason, failed to handle
     *    the request.
     */
    @GetMapping
    fun listActions(pageable: Pageable): Paginated<ActionDto> {
        val actions = actionService.listActions(pageable).map { it.toDto() }
        return Paginated(actions)
    }

    /**
     * Returns a detailed view for the requested action. Possible responses are:
     *  - 200 OK: When we successfully managed to find the requested action.
     *  - 403 Forbidden: Client does not posses the required permissions.
     *  - 404 Not Found: When the requested action does not exist.
     *  - 500 Internal Server Error: Server, for whatever reason, failed to handle
     *    the request.
     */
    @GetMapping("/{name}")
    fun getAction(@PathVariable name: String) =
        actionService.getAction(name)?.toDto() ?: throw ActionNotFoundException(name)
}

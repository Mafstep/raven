package ir.jibit.raven.web.action

import ir.jibit.raven.model.action.Parameter
import ir.jibit.raven.service.action.ParameterNotFoundException
import ir.jibit.raven.service.action.ParameterService
import ir.jibit.raven.web.Paginated
import ir.jibit.raven.web.action.dto.ParameterDto
import ir.jibit.raven.web.action.dto.toDto
import ir.jibit.raven.web.action.dto.toEntity
import ir.jibit.raven.web.parameters
import ir.jibit.raven.web.validator.ValidationSeq
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus.NO_CONTENT
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*

/**
 * Provides a REST API to manage action parameters:
 *  - [parameterService] Will help us to manage parameters.
 *
 * @author Mehran Behnam
 */
@RestController
@RequestMapping(parameters)
class ParameterController(private val parameterService: ParameterService) {

    /**
     * REST endpoint responsible for creating a parameter. Possible responses are:
     *  - 204 No Content: When the parameter created successfully.
     *  - 400 Bad Request: When the given parameters are invalid.
     *  - 403 Forbidden: When the client does not possess the required permissions.
     *  - 500 Internal Server Error: When server failed to handle the request.
     */
    @PostMapping
    @ResponseStatus(NO_CONTENT)
    fun createParameter(@RequestBody @Validated(ValidationSeq::class) dto: ParameterDto) {
        parameterService.createParameter(dto.toEntity())
    }

    /**
     * REST endpoint responsible for returning a paginated list of parameters:
     *  - 200 OK: When we managed to successfully return the requested list.
     *  - 403 Forbidden: When the client does not possess the required permissions.
     *  - 500 Internal Server Error: When the server failed to handle the request properly.
     */
    @GetMapping
    fun listParameters(pageable: Pageable): Paginated<Parameter> {
        return Paginated(parameterService.listParameters(pageable))
    }

    /**
     * REST endpoint responsible for returning the details for a specific parameter. Possible
     * responses are:
     *  - 200 OK: The requested parameter is found and going to be serialized as the body of a 200
     *            OK response.
     *  - 403 Forbidden: The client does not posses the required permissions.
     *  - 404 Not Found: The requested parameter does not exist.
     *  - 500 Internal Server Error: When server failed to handle the request.
     */
    @GetMapping("/{name}")
    fun getParameter(@PathVariable name: String) =
        parameterService.getParameter(name)?.toDto() ?: throw ParameterNotFoundException(name)
}

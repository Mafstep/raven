package ir.jibit.raven.web.error

import me.alidg.errors.ExceptionRefiner
import org.springframework.core.convert.ConversionFailedException
import org.springframework.stereotype.Component
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException

/**
 * Transforms some exceptions to another exceptions before handling them.
 *
 * @author Mehran Behnam
 */
@Component
class RavenExceptionRefiner : ExceptionRefiner {

    /**
     * When we throw an exception inside a [org.springframework.core.convert.converter.Converter],
     * the thrown exception would get wrapped inside either [MethodArgumentTypeMismatchException]
     * or [ConversionFailedException] depending on the context. Here we just unwrap those exceptions
     * to handle the cause not the symptom.
     */
    override fun refine(exception: Throwable?): Throwable? = when (exception) {
        is ConversionFailedException -> exception.cause
        is MethodArgumentTypeMismatchException -> exception.cause?.cause
        else -> exception
    }
}

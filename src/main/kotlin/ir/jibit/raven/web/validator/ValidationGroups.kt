package ir.jibit.raven.web.validator

import javax.validation.GroupSequence

/**
 * A validation group we'd be using to ensure that the annotated field has populated
 * with some value.
 *
 * **Note**: Only use this annotation on DTO fields.
 *
 * [Grouping constraints](https://red.ht/2kVL1Ix)
 *
 * @author Mehran Behnam
 */
interface Presence

/**
 * A validation groups we'd be using to ensure that the populated content is valid.
 *
 * **Note**: Only use this annotation on DTO fields.
 *
 * @author Mehran Behnam
 */
interface Validity

/**
 * A validation group we'd be using to ensure that the validation logic should be applied
 * after all other validation groups.
 *
 * **Note**: Only use this annotation on DTO fields.
 */
interface Late

/**
 * A validation sequence that ensures first we validate fields annotated with [Presence]
 * group and then [Validity] group. That is, if all properties in the [Presence] group
 * were valid, then we apply validations of [Validity] group.
 *
 * **Note**: Only use this annotation as an argument for
 * [org.springframework.validation.annotation.Validated] annotation in controller methods.
 *
 * @author Mehran Behnam
 */
@GroupSequence(Presence::class, Validity::class, Late::class)
interface ValidationSeq

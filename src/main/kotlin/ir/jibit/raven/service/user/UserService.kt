package ir.jibit.raven.service.user

import ir.jibit.raven.model.user.User

/**
 * Provides a set of services to manage users.
 *
 * @author Nazila Akbari
 * @author Mehran Behnam
 */
interface UserService {

    /**
     * Verifies the given token and returns the corresponding user details.
     */
    fun findByToken(token: String): User?

    /**
     * Returns the current authenticated user or `null`.
     */
    fun currentAuthenticatedUser(): User?
}

package ir.jibit.raven.service.user

import ir.jibit.raven.model.user.User
import ir.jibit.raven.util.logger
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpHeaders.AUTHORIZATION
import org.springframework.http.HttpMethod.GET
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.exchange

/**
 * An Iran Pay based implementation for [UserService]. The Main operation in this service is [findByToken]
 * and it's using an Iran Pay API to verify the given token, hence the implementation name.
 *
 * @author Nazila Akbari
 */
@Service
@ConfigurationProperties(prefix = "app-server.user-profile")
class IranPayUserService(private val restTemplate: RestTemplate) : UserService {

    /**
     * Logger.
     */
    private val log = logger<IranPayUserService>()

    /**
     * Represents the base URL of token verification service in Iran Pay.
     */
    lateinit var url: String

    /**
     * Querying Iran Pay to get user profile details out of the given [token]. Returns the
     * requested user details or `null` when there is no such user.
     */
    override fun findByToken(token: String): User? {
        val request = prepareRequest(token)
        try {
            val response = restTemplate.exchange<IranPayResponse>(url = url, method = GET, requestEntity = request)
            log.debug("Got a response from Iran Pay for token verification: {}", response)

            if (response.statusCode.is2xxSuccessful) {
                return response.body?.data?.toUser()
            }
        } catch (e: Throwable) {
            log.warn("Failed to send a request to Iran Pay for token verification", e)
        }

        return null
    }

    /**
     * Retrieves the current authenticated user from Spring Security Context. Also it may
     * return `null` when the client is unauthenticated.
     */
    override fun currentAuthenticatedUser() = try {
        User(SecurityContextHolder.getContext().authentication.name)
    } catch (e: Throwable) {
        null
    }

    private fun prepareRequest(token: String): HttpEntity<String> {
        val headers = HttpHeaders()
        headers.set(AUTHORIZATION, token)
        headers.set("Device", null)

        return HttpEntity(headers)
    }

    /**
     * Encapsulates a successful response from Iran Pay profile API.
     */
    private class IranPayResponse(var data: Profile)

    /**
     * The profile information.
     */
    private class Profile(var mobile: String) {

        /**
         * Converts Iran Pay user's model to our representation of a user.
         */
        fun toUser(): User = User(mobile)
    }
}

package ir.jibit.raven.service.notification

import ir.jibit.raven.model.notification.Notification
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Slice
import java.util.*

/**
 * Defines a contract to manage notifications.
 *
 * @author Mehran Behnam
 */
interface NotificationService {

    /**
     * Sends the given [notification] to its intended destination.
     *
     * @throws [ir.jibit.raven.notification.NotificationProcessingFailureException] When we fail to send
     *         the notification.
     */
    fun sendNotification(notification: Notification)

    /**
     * Retrieves the notification identified by the given [id] from the Datasource or returns `null`.
     */
    fun getNotification(id: UUID): Notification?

    /**
     * Returns a paginated collection of recent notifications. [pageable] encapsulates the pagination details.
     *
     * @throws IllegalArgumentException If the given [pageable] is invalid.
     */
    fun latestNotifications(pageable: Pageable): Slice<Notification>
}

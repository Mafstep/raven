package ir.jibit.raven.service.notification

import ir.jibit.raven.model.notification.Notification
import ir.jibit.raven.notification.NotificationProcessor
import ir.jibit.raven.repository.NotificationRepository
import ir.jibit.raven.util.isValid
import ir.jibit.raven.util.logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import java.util.*

/**
 * A [NotificationService] implementation which only publishes the given notifications into
 * appropriate queues and does not much else:
 *  - [notificationProcessors] List of available [NotificationProcessor]s.
 *  - [notificationRepository] Provides data access operations over notifications.
 * @author Mehran Behnam
 */
@Service
class QueuingNotificationService(@Autowired(required = false) private val notificationProcessors: List<NotificationProcessor>?,
                                 private val notificationRepository: NotificationRepository) : NotificationService {
    /**
     * Plain old logger.
     */
    private val log = logger<QueuingNotificationService>()

    /**
     * Persists the given notification and publishes it to an appropriate message queue.
     */
    override fun sendNotification(notification: Notification) {
        log.debug("About to process a notification: {}", notification.id)
        notificationRepository.save(notification)
        log.debug("Just persisted the {} notification", notification.id)

        val sender = notificationProcessors?.firstOrNull { it.canProcess(notification) }
        if (sender != null) {
            try {
                sender.process(notification)
                log.info("The notification has been submitted successfully: {}", notification.id)
            } catch (e: Throwable) {
                log.error("Failed to send a notification", e)
                throw e
            }
        } else {
            log.warn("There is no notification processor for {}", notification.id)
        }
    }

    /**
     * Retrieves the notification identified by the given [id] from the Datasource or returns `null`.
     */
    override fun getNotification(id: UUID): Notification? = notificationRepository.findById(id).orElse(null)

    /**
     * Retrieves a paginated list of notifications.
     */
    override fun latestNotifications(pageable: Pageable) =
        if (!pageable.isValid())
            throw IllegalArgumentException("The given pagination information is invalid: $pageable")
        else
            notificationRepository.latestNotifications(pageable)
}

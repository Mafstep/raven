package ir.jibit.raven.service.action

import ir.jibit.raven.model.action.Parameter
import ir.jibit.raven.repository.ParameterRepository
import ir.jibit.raven.util.isValid
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Slice
import org.springframework.stereotype.Service

/**
 * A simple repository-oriented implementation of [ParameterService].
 *
 * @author Mehran Behnam
 */
@Service
class SimpleParameterService(private val parameterRepository: ParameterRepository) : ParameterService {

    /**
     * Persists the given parameter or throws [ParameterAlreadyExistsException] when the parameter is
     * duplicate.
     */
    override fun createParameter(parameter: Parameter) {
        if (getParameter(parameter.name) != null) throw ParameterAlreadyExistsException(parameter.name)
        parameterRepository.save(parameter)
    }

    /**
     * Retrieves the given parameter or `null` when there is no such thing.
     */
    override fun getParameter(name: String): Parameter? = parameterRepository.findById(name).orElse(null)

    /**
     * Returns a paginated list of parameters.
     */
    override fun listParameters(pageable: Pageable): Slice<Parameter> =
        if (pageable.isValid()) parameterRepository.findAll(pageable)
        else throw IllegalArgumentException("The given pagination information is invalid")
}

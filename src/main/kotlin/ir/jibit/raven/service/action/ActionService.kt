package ir.jibit.raven.service.action

import ir.jibit.raven.model.action.Action
import ir.jibit.raven.model.action.ActionEvent
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Slice

/**
 * Provides CRUD-ish services to manage action and action events.
 *
 * @author Mehran Behnam
 */
interface ActionService {

    /**
     * Tries to find an action with the given [name] or return `null` when no such action
     * exists.
     */
    fun getAction(name: String): Action?

    /**
     * Returns a paginated list of actions. The pagination information is encapsulated the
     * given [pageable].
     *
     * @throws IllegalArgumentException When the given pagination information is invalid.
     */
    fun listActions(pageable: Pageable): Slice<Action>

    /**
     * Would create and persist the given [action].
     *
     * @throws IllegalArgumentException For simple and static precondition violations.
     * @throws ActionAlreadyExistsException When another action with the same name already exists.
     * @throws InvalidActionRuleFallbackReferenceException When some fallback references are pointing
     *         to an invalid notification type.
     * @throws ParameterNotFoundException When at least one parameter is invalid and does not exist.
     */
    fun createAction(action: Action)

    /**
     * Updates the action identified by the [Action.name] by the information encapsulated in the given
     * [action].
     *
     * @throws InvalidActionRuleFallbackReferenceException When some fallback references are pointing
     *         to an invalid notification type.
     * @throws ParameterNotFoundException When at least one parameter is invalid and does not exist.
     */
    fun updateAction(action: Action)

    /**
     * Submits the given action event and published its notifications.
     */
    fun submitEvent(actionEvent: ActionEvent)
}

package ir.jibit.raven.service.action

import ir.jibit.raven.model.action.Parameter
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Slice

/**
 * Provides CRUD services for action parameters.
 *
 * @author Mehran Behnam
 */
interface ParameterService {

    /**
     * Persists the given parameter.
     *
     * @throws ParameterAlreadyExistsException When there is another parameter with the same name.
     */
    fun createParameter(parameter: Parameter)

    /**
     * Retirees a parameter identified by the given name or returns `null`.
     */
    fun getParameter(name: String): Parameter?

    /**
     * Returns a paginated list of available parameters.
     *
     * @throws IllegalArgumentException If the given [pageable] is invalid.
     */
    fun listParameters(pageable: Pageable): Slice<Parameter>
}

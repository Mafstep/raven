package ir.jibit.raven.service.action

import ir.jibit.raven.model.notification.NotificationType
import me.alidg.errors.annotation.ExceptionMapping
import me.alidg.errors.annotation.ExposeAsArg
import org.springframework.http.HttpStatus.BAD_REQUEST
import org.springframework.http.HttpStatus.NOT_FOUND

/**
 * Raised when action with [actionName] already existed in our Datasource.
 *
 * @author Mehran Behnam
 */
@ExceptionMapping(statusCode = BAD_REQUEST, errorCode = "action.already_exists")
class ActionAlreadyExistsException(@get:ExposeAsArg(0) val actionName: String) : RuntimeException()

/**
 * Raised when the action rule fallback reference is invalid.
 *
 * @see [ir.jibit.raven.model.action.ActionRule.fallbackFor]
 *
 * @author Mehran Behnam
 */
@ExceptionMapping(statusCode = BAD_REQUEST, errorCode = "action.rule.invalid_fallback")
class InvalidActionRuleFallbackReferenceException(@get:ExposeAsArg(0) val invalidFallbacks: Set<NotificationType>)
    : RuntimeException()

/**
 * Raised when we couldn't find the requested action.
 *
 * @author Mehran Behnam
 */
@ExceptionMapping(statusCode = NOT_FOUND, errorCode = "action.not_found")
class ActionNotFoundException(@get:ExposeAsArg(0) val actionName: String) : RuntimeException()

package ir.jibit.raven.service.action

import me.alidg.errors.annotation.ExceptionMapping
import me.alidg.errors.annotation.ExposeAsArg
import org.springframework.http.HttpStatus.BAD_REQUEST
import org.springframework.http.HttpStatus.NOT_FOUND

/**
 * Raised when there is no parameter named [parameterName] in the Datasource.
 *
 * @author Mehran Behnam
 */
@ExceptionMapping(statusCode = NOT_FOUND, errorCode = "action.parameter.not_found")
class ParameterNotFoundException(@get:ExposeAsArg(0) val parameterName: String) : RuntimeException()

/**
 * Raised when another parameter with the same name already exists and we can't create another
 * one with the same name.
 *
 * @author Mehran Behnam
 */
@ExceptionMapping(statusCode = BAD_REQUEST, errorCode = "parameter.already_exists")
class ParameterAlreadyExistsException(@get:ExposeAsArg(0) val parameterName: String) : RuntimeException()

package ir.jibit.raven.service.action

import ir.jibit.raven.model.action.Action
import ir.jibit.raven.model.action.ActionEvent
import ir.jibit.raven.repository.ActionRepository
import ir.jibit.raven.repository.ParameterRepository
import ir.jibit.raven.util.isValid
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import java.lang.IllegalArgumentException

/**
 * Simple repository-oriented [ActionService] implementation:
 *  - [actionRepository] Provides data access operations over [Action] entity.
 *  - [parameterRepository] Provides data access operations over parameters.
 *
 * @author Mehran Behnam
 */
@Service
class SimpleActionService(private val actionRepository: ActionRepository,
                          private val parameterRepository: ParameterRepository) : ActionService {

    /**
     * Returns an [Action] with the given [name] or `null`.
     */
    override fun getAction(name: String): Action? = actionRepository.findById(name).orElse(null)

    /**
     * Returns a paginated list of [Action]s or throws an exception when the pagination information is
     * invalid.
     */
    override fun listActions(pageable: Pageable) =
        if (pageable.isValid()) actionRepository.findAll(pageable)
        else throw IllegalArgumentException("Pagination information is invalid: $pageable")

    /**
     * Validates the given [action] and if were valid, persists it into the Datasource.
     */
    override fun createAction(action: Action) {
        require(action.actionRules.isNotEmpty()) { "Action requires at least one action rule" }
        failIfActionAlreadyExists(action)
        failIfActionRuleFallbacksAreInvalid(action)

        val parameters = loadParameters(action)
        val newAction = Action(action.name, parameters, action.actionRules)
        actionRepository.save(newAction)
    }

    override fun updateAction(action: Action) {
        TODO()
    }

    override fun submitEvent(actionEvent: ActionEvent) {
        TODO()
    }

    /**
     * If another action exists with the same name, throws [ActionAlreadyExistsException].
     */
    private fun failIfActionAlreadyExists(action: Action) {
        if (actionRepository.findById(action.name).isPresent) {
            throw ActionAlreadyExistsException(action.name)
        }
    }

    /**
     * If there is at least one invalid fallback reference, throws [InvalidActionRuleFallbackReferenceException].
     */
    private fun failIfActionRuleFallbacksAreInvalid(action: Action) {
        val references = action.findInvalidFallbackReferences()
        if (references.isNotEmpty()) {
            throw InvalidActionRuleFallbackReferenceException(references)
        }
    }

    /**
     * Load the encapsulated parameters in the given [action] or throws a [ParameterNotFoundException]
     * if at least one action is invalid.
     */
    private fun loadParameters(action: Action) =
        action.parameters.map {
            parameterRepository.findById(it.name).orElseThrow { ParameterNotFoundException(it.name) }
        }.toSet()
}

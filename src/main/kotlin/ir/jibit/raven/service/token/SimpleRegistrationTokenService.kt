package ir.jibit.raven.service.token

import ir.jibit.raven.model.token.RegistrationToken
import ir.jibit.raven.repository.RegistrationTokenRepository
import org.springframework.stereotype.Service

/**
 * A simple [RegistrationTokenService] which provides CRUD-ish services for device tokens.
 *
 * @author Nazila Akbari
 */
@Service
class SimpleRegistrationTokenService(private val registrationTokenRepository: RegistrationTokenRepository)
    : RegistrationTokenService {

    /**
     * Translates the given user identifiers (Phone Numbers) to their tokens.
     */
    override fun getDeviceTokens(userIdentifiers: Set<String>): Set<RegistrationToken> {
        return registrationTokenRepository.findAllByUserIn(userIdentifiers)
    }

    /**
     * Simply persist the given [registrationToken] to the database.
     */
    override fun registerToken(registrationToken: RegistrationToken): RegistrationToken {
        return registrationTokenRepository.save(registrationToken)
    }
}

package ir.jibit.raven.service.token

import ir.jibit.raven.model.token.RegistrationToken

/**
 * Provides a set of services to manage device tokens.
 *
 * @author Nazila Akbari
 * @author Mehran Behnam
 */
interface RegistrationTokenService {

    /**
     * Persists the given [registrationToken] into database.
     */
    fun registerToken(registrationToken: RegistrationToken): RegistrationToken

    /**
     * Returns [RegistrationToken]s for the given [userIdentifiers], i.e. Phone Number.
     */
    fun getDeviceTokens(userIdentifiers: Set<String>): Set<RegistrationToken>

}

-- Creates a table to store notifications
CREATE TABLE notifications(
  id CHAR(36) PRIMARY KEY,
  notification_type VARCHAR(20) NOT NULL,
  data TEXT NULL,
  ttl INTEGER NOT NULL DEFAULT -1,
  notification_state VARCHAR(20) NOT NULL,
  process_log TEXT NULL,
  recipient_tokens TEXT NULL,
  topic TEXT NULL,
  title TEXT NULL,
  recipients TEXT NULL,
  subject TEXT NULL,
  body TEXT NULL,
  message TEXT NULL,
  when_submitted TIMESTAMP NOT NULL
) ENGINE=InnoDB;
create index notifications_when_submitted_index on notifications (when_submitted desc);

-- Creates a table to maintain action parameters
CREATE TABLE parameters(
  name VARCHAR(30) PRIMARY KEY,
  type VARCHAR(30) NOT NULL
) ENGINE=InnoDB;

-- Creates a table to maintain actions
CREATE TABLE actions(
  name VARCHAR(30) PRIMARY KEY
) ENGINE=InnoDB;

-- Creates a table to maintain action rules
CREATE TABLE action_rules(
  action_name VARCHAR(30) NOT NULL,
  notification_type VARCHAR(20) NOT NULL,
  fallback_for VARCHAR(20) NULL,
  PRIMARY KEY (action_name, notification_type),
  CONSTRAINT action_rules_actions_name_fk
  FOREIGN KEY (action_name) REFERENCES actions (name) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

-- Creates a table to maintain action parameters
CREATE TABLE action_parameters(
  action_name VARCHAR(30) NOT NULL,
  parameter_name VARCHAR(30) NOT NULL,
  PRIMARY KEY (action_name, parameter_name),
  CONSTRAINT action_parameters_actions_name_fk
  FOREIGN KEY (action_name) REFERENCES actions (name) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT action_parameters_parameters_name_fk
  FOREIGN KEY (parameter_name) REFERENCES parameters (name) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

-- Creates a table to maintain action events
CREATE TABLE action_events(
  id CHAR(36) PRIMARY KEY,
  action_name VARCHAR(30) NOT NULL,
  action_parameters TEXT NOT NULL,
  user VARCHAR(100) NOT NULL,
  when_happened TIMESTAMP NOT NULL,
  CONSTRAINT action_events_actions_name_fk
  FOREIGN KEY (action_name) REFERENCES actions (name)
) ENGINE=InnoDB;

-- Creates a table to maintain the relationship between actions and notifications
CREATE TABLE action_event_notifications(
  action_event_id CHAR(36) NOT NULL,
  notification_id CHAR(36) NOT NULL,
  PRIMARY KEY (action_event_id, notification_id),
  CONSTRAINT action_event_notifications_action_events_id_fk
  FOREIGN KEY (action_event_id) REFERENCES action_events (id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT action_event_notifications_notifications_id_fk
  FOREIGN KEY (notification_id) REFERENCES notifications (id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

-- Creates a table to maintain registration tokens for different devices and targets
CREATE TABLE registration_tokens(
  id CHAR(36) PRIMARY KEY,
  user VARCHAR(100) NOT NULL,
  token TEXT NOT NULL,
  token_type VARCHAR(20) NOT NULL,
  app_version VARCHAR(20) NOT NULL,
  metadata TEXT NULL,
  registration_date TIMESTAMP NOT NULL
) ENGINE=InnoDB;
CREATE INDEX registration_tokens_user_index ON registration_tokens (user);
